import React from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

interface DNAppNavbarActionProps {
    name: string;
    link: string;
}

export default class DNAppNavbarAction extends React.Component<DNAppNavbarActionProps, {}> {
    constructor(props: DNAppNavbarActionProps) {
        super(props);
    }

    render() {
        return (
            <NavLink to={this.props.link} className = "DNAppNavbarElement">
                <button className="dt_button DNAppNavbarAction">{this.props.name}</button>
            </NavLink>
        )
    }
}