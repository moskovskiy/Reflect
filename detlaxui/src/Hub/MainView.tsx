import React from 'react';
import DTAppContent from '../components/DTAppContent/DTAppContent'
import DTAppContentHeader from '../components/DTAppContent/DTAppContentHeader'

import DNAppNavbar from '../components/common/DNAppNavbar/DNAppNavbar'
import DNAppNavbarMenu from '../components/common/DNAppNavbar/DNAppNavbarMenu'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"
import { DNAppContent } from '../components/common/DNAppContent/DNAppContent'

const HubMainView: React.FC = () => {
    document.title = 'Дашборд - Detlax';
    return <div className ="HubMainView">
            <DNAppNavbar>
            <DNAppNavbarMenu/>
            </DNAppNavbar>
            <DNAppContent
            header="Умный хаб">
                
            </DNAppContent>
    </div>
}

export default HubMainView