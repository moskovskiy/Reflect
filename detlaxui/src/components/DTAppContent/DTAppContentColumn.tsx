import React from 'react';
import './DTAppContent.css';

const DTAppContentColumn: React.FC = (props) => {
    return <div className ="DTAppContentColumn">
        <div className="DTAppContentColumnView">
            {props.children}
        </div>
    </div>
}

export default DTAppContentColumn