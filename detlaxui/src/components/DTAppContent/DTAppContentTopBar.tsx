import React from 'react';
import './DTAppContent.css';

const DTAppContentColumn: React.FC = (props) => {
    return <div className ="DTAppContentTopBar">
            {props.children}
    </div>
}

export default DTAppContentColumn