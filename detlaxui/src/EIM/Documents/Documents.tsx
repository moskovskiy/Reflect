import * as React from 'react';
import DTAppContent from '../../components/DTAppContent/DTAppContent'
import DTAppContentHeader from '../../components/DTAppContent/DTAppContentHeader'
import DTTable from '../../components/DTTable/DTTable'
import { BrowserRouter as Router, Route, NavLink, Redirect, useHistory } from "react-router-dom"


// New components import 
import { DNAppContent } from '../../components/common/DNAppContent/DNAppContent'
import DNAppMenu from '../../components/common/DNAppMenu/DNAppMenu'
import DNAppMenuHeader from '../../components/common/DNAppMenu/DNAppMenuHeader'
import DNAppMenuTab from '../../components/common/DNAppMenu/DNAppMenuTab'
import DNAppMenuBack from '../../components/common/DNAppMenu/DNAppMenuBack'


import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';

import EIMDocumentView from './Document'

const GetDocumentGraphQLQuery = gql`
  query {
	  eimQuery(name: "eim.Document.Query.GetAllDocumentsPreview", payload: "{}")
  } 
`

interface Document {
    id: string;
    name: string;
    contentpreview: string;
    dateLM: string;
    children: React.ReactNode
}

interface AppState {
    count: number;
}

interface AppProps {
    steps: number;
    setdocumentToSearchRef: Element;
}

export default class EIMDocumentsView extends React.Component<AppProps, AppState> {
    private documentToSearch: React.RefObject<HTMLInputElement>;

    constructor(props: AppProps) {
        super(props);
        this.state = {
            count: 0
        };
        this.documentToSearch = React.createRef();
    }

    incrementCounter() {
        this.setState({count: this.state.count + 1});
    }

    render() {
    document.title = 'Все документы - Detlax';

    return (
    <div className ="EIMDocumentsView">

        <Route exact path = "/eim/documents/list/:id" component={EIMDocumentView} />
        <Route exact path = "/eim/documents" render={() => <Redirect to="/eim/documents/list" />} />
        <DNAppMenu>
            <DNAppMenuTab
                link={"/eim/documents/list"}
                name="Документы"
                icon={
                    <>
                        <path d="M8 16h8v2H8zm0-4h8v2H8zm6-10H6c-1.1 0-2 .9-2 2v16c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11z"/>
                    </>
                }
            />

            <DNAppMenuTab
                link={"/eim/documents/tosign"}
                name="Ожидают действия"
                icon={
                    <>    
                        <path d="M0 0h24v24H0V0z" fill="none"/><path d="M4.59 6.89c.7-.71 1.4-1.35 1.71-1.22.5.2 0 1.03-.3 1.52-.25.42-2.86 3.89-2.86 6.31 0 1.28.48 2.34 1.34 2.98.75.56 1.74.73 2.64.46 1.07-.31 1.95-1.4 3.06-2.77 1.21-1.49 2.83-3.44 4.08-3.44 1.63 0 1.65 1.01 1.76 1.79-3.78.64-5.38 3.67-5.38 5.37 0 1.7 1.44 3.09 3.21 3.09 1.63 0 4.29-1.33 4.69-6.1H21v-2.5h-2.47c-.15-1.65-1.09-4.2-4.03-4.2-2.25 0-4.18 1.91-4.94 2.84-.58.73-2.06 2.48-2.29 2.72-.25.3-.68.84-1.11.84-.45 0-.72-.83-.36-1.92.35-1.09 1.4-2.86 1.85-3.52.78-1.14 1.3-1.92 1.3-3.28C8.95 3.69 7.31 3 6.44 3 5.12 3 3.97 4 3.72 4.25c-.36.36-.66.66-.88.93l1.75 1.71zm9.29 11.66c-.31 0-.74-.26-.74-.72 0-.6.73-2.2 2.87-2.76-.3 2.69-1.43 3.48-2.13 3.48z"/>
                    </>
                }
            />

            <NavLink to="/eim/documents/starred" className="DTAppMenuNavLink" activeClassName="DTAppMenuNavLinkActive">
                <button className="DTAppMenuNavigate">
                    <svg className="DTAppMenuNavigateIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12.36 6l.4 2H18v6h-3.36l-.4-2H7V6h5.36M14 4H5v17h2v-7h5.6l.4 2h7V6h-5.6L14 4z"/></svg>
                    <span className="DTAppMenuNavigateText">Отмеченные</span>
                </button>
            </NavLink>

            <NavLink to="/eim/documents/types" className="DTAppMenuNavLink" activeClassName="DTAppMenuNavLinkActive">
                <button className="DTAppMenuNavigate">
                    <svg className="DTAppMenuNavigateIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M2.53 19.65l1.34.56v-9.03l-2.43 5.86c-.41 1.02.08 2.19 1.09 2.61zm19.5-3.7L17.07 3.98c-.31-.75-1.04-1.21-1.81-1.23-.26 0-.53.04-.79.15L7.1 5.95c-.75.31-1.21 1.03-1.23 1.8-.01.27.04.54.15.8l4.96 11.97c.31.76 1.05 1.22 1.83 1.23.26 0 .52-.05.77-.15l7.36-3.05c1.02-.42 1.51-1.59 1.09-2.6zm-9.2 3.8L7.87 7.79l7.35-3.04h.01l4.95 11.95-7.35 3.05z"/><circle cx="11" cy="9" r="1"/><path d="M5.88 19.75c0 1.1.9 2 2 2h1.45l-3.45-8.34v6.34z"/></svg>
                    <span className="DTAppMenuNavigateText">Типы документов</span>
                </button>
            </NavLink>
        </DNAppMenu>

        <Route exact path = "/eim/documents/list">
            <DNAppContent hasMenu={true}
                header="Документы"
            >
                    <div className="DTAppContentPanel">
                        <input type="text" className="dt_input" ref={this.documentToSearch} placeholder="Уникальный ID"></input>
                        <NavLink to={() => {return "/eim/documents/" + this.documentToSearch.current}}>
                            <span className="dt_button dt_button-secondary DTAppContentPanelElementContainerMainLink">
                                <span className="DTAppContentPanelElement">Поиск</span>
                            </span>
                        </NavLink>

                        <button onClick={()=>{alert("Выбор сейчас недоступен")}} className="dt_button dt_button-inline DTAppContentPanelElementContainer">
                            <svg className="DTAppContentPanelElementSVG" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                                <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-8.29 13.29c-.39.39-1.02.39-1.41 0L5.71 12.7c-.39-.39-.39-1.02 0-1.41.39-.39 1.02-.39 1.41 0L10 14.17l6.88-6.88c.39-.39 1.02-.39 1.41 0 .39.39.39 1.02 0 1.41l-7.58 7.59z"/>
                            </svg>
                            <span className="DTAppContentPanelElement">Выбрать</span>
                        </button>
                        <button onClick={()=>{alert("Сортировка сейчас недоступна")}} className="dt_button dt_button-inline DTAppContentPanelElementContainer">
                            <svg className="DTAppContentPanelElementSVG" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                                <path d="M3 18h6v-2H3v2zM3 6v2h18V6H3zm0 7h12v-2H3v2z"/>
                            </svg>
                            <span className="DTAppContentPanelElement">Сортировать таблицу</span>
                        </button>
                        <button onClick={()=>{alert("Скачивание сейчас недоступно")}} className="dt_button dt_button-inline DTAppContentPanelElementContainer">
                            <svg className="DTAppContentPanelElementSVG" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                                <path d="M19 12v7H5v-7H3v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7h-2zm-6 .67l2.59-2.58L17 11.5l-5 5-5-5 1.41-1.41L11 12.67V3h2z"></path>
                            </svg>
                            <span className="DTAppContentPanelElement">Сохранить в CSV</span>
                        </button>
                    </div>
                    <DTTable>
                        <tr className="DTTableHeadRow">
                            {/*<th className="DTTableElement DTTableHead">
                                Уникальный идентификатор
                            </th>*/}
                            <th className="DTTableElement DTTableHead">
                                
                            </th>
                            <th className="DTTableElement DTTableHead">
                                Название
                            </th>
                            <th className="DTTableElement DTTableHead">
                                Предпросмотр
                            </th>
                            <th className="DTTableElement DTTableHead">
                                Текущий этап
                            </th>
                            <th className="DTTableElement DTTableHead">
                                Дата последнего изменения
                            </th>
                        </tr>
                        <EIMDocumentsLoader/>
                    </DTTable>
                </DNAppContent>
            </Route>
            <Route exact path = "/eim/documents/types">
                <DNAppContent
                    hasMenu={true}
                    header="Типы документов"
                >
                    Список типов документов
                </DNAppContent>
            </Route>
    </div>
    );
    }
}

const EIMDocumentsLoader: React.FC = () => {
    let { loading, error, data } = useQuery(GetDocumentGraphQLQuery);

    if (loading) {
        return (<tr><td className="DTTableElement"><div className="dt_loader"></div></td></tr>)
    }
    if (error) {
        return (<tr><td className="DTTableElement">{error.message}</td></tr>)
    }
    var datamap: Document[]
    try {
        datamap = JSON.parse(data.eimQuery).documents
        return (<EIMDocumentsList datamap={datamap}/>)
    } catch(e) {
        console.log(e)
        return (<tr><td className="DTTableElement">
            Error parsing server response.
        </td></tr>)
    }
}

const EIMDocumentCard: React.FC<{ card: Document }> = ({ card }) => {
    let history = useHistory();
    return <>
            <tr className="DTTableRow dt_selectable" key={card.id} onClick={() => {document.location.href="/eim/documents/list/"+card.id}}>
                {/*<td className="DTTableElement DTTableElementMono dt_text-mono">{card.id}</td>*/}
                <td className="DTTableElement">
                    <a href={"/eim/documents/list/" + card.id} className="dt_button dt_button-large">
                        <svg className="DTTableElementIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 19H5V5h7V3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2v-7h-2v7zM14 3v2h3.59l-9.83 9.83 1.41 1.41L19 6.41V10h2V3h-7z"/></svg>
                    </a>
                </td>
                <td className="DTTableElement">{card.name}</td>
                <td className="DTTableElement">{card.contentpreview}</td>
                <td className="DTTableElement">{"Приказ заверен у начальника"}</td>
                <td className="DTTableElement">{card.dateLM}</td>
            </tr>
        </>
}

const EIMDocumentsList: React.FC<{ datamap: Document[] }> = ({ datamap }) => {
    return (
        (datamap.length == 0) ? <><td className="DTTableElement">Документов нет</td></>: <>
        {datamap.map(document => {
            return (<EIMDocumentCard key={document.id} card={document} />)
        })}
        </>
    )
}

