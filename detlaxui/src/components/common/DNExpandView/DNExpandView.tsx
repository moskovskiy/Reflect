/* ---------------------------

    App Menu Navigation link
    @description: Provides NavLink with sent components

    Part of the Reflect UI system

---------------------------- */

// React imports
import React from 'react';
import './DNExpandView.css'

interface ExpandProps {
    label: string;
    labelElement: any;
    expandOpenHint: string;
    expandCloseHint: string;
    closed: any;
    opened: any;
}

type ExpandState = {
    rotation: number;
    closed: boolean;
}

export default class DNExpandView extends React.Component<ExpandProps, ExpandState> {

    constructor(props: ExpandProps) {
        super(props);
    }

    toggle() {
        this.setState({
            rotation: ((this.state.rotation + 180)%360),
            closed: !this.state.closed
        });
    }

    componentWillMount() {
        this.setState({
            closed: true,
            rotation: 0
        });
    }

    render() {
        return (
            <>
                <div className="DNExpandViewHead">
                    <span className="DNExpandViewHeader">{this.props.label}</span>
                    <span className="DNExpandViewHeader">{this.props.labelElement}</span>
                    <button className="dt_button DNExpandViewExpand" onClick={()=>{this.toggle()}}>   
                        {this.state.closed && <div className="DNExpandViewExpandHint">{this.props.expandOpenHint}</div>}
                        {(!this.state.closed) && <div className="DNExpandViewExpandHint">{this.props.expandCloseHint}</div>}
                        <svg transform={"rotate(" + this.state.rotation + ")"} className="DNExpandViewExpandIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"/></svg>
                    </button>
                </div>
                {this.state.closed && <div className="DNExpandViewContent">{this.props.closed}</div>}
                {(!this.state.closed) && <div className="DNExpandViewContent">{this.props.opened}</div>}
            </>
        );
    }
}