import React from 'react';
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

import DTAppContentHeaderLarge from '../components/DTAppContent/DTAppContentHeaderLarge'

import EIMWorkflowSpaceView from './Workflows/Workflows'
import EIMWorkflowView from './Workflows/Workflow'

import EIMTemplateView from './Templates/Template'

import EIMDocumentsView from './Documents/Documents'

import EIMCreateView from './Create/Create'

import Avatar from '../assets/avatar.png'

import DNAppNavbar from '../components/common/DNAppNavbar/DNAppNavbar'
import DNAppNavbarMenu from '../components/common/DNAppNavbar/DNAppNavbarMenu'
import DNAppNavbarLink from '../components/common/DNAppNavbar/DNAppNavbarLink'
import DNAppNavbarAction from '../components/common/DNAppNavbar/DNAppNavbarAction'
import { DNAppContent } from '../components/common/DNAppContent/DNAppContent'
import DNAppNavbarDropdown from '../components/common/DNAppNavbar/DNAppNavbarDropdown'
import DNAppNavbarDropdownElement from '../components/common/DNAppNavbar/DNAppNavbarDropdownElement'


const EIMMainView: React.FC = () => {
    return <div className ="EIMMainView">
        <DNAppNavbar>

            <DNAppNavbarMenu/>
            <DNAppNavbarLink
                name="Документы"
                iconRight={<></>}
                favicon={<>
                    <path d="M8 16h8v2H8zm0-4h8v2H8zm6-10H6c-1.1 0-2 .9-2 2v16c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11z"/>
                </>}
                link="/eim/documents"
            />

            <DNAppNavbarLink
                name="Процессы"
                iconRight={<></>}
                favicon={<>
                   <path d="M7,19h10V4H7V19z M9,6h6v11H9V6z"/><rect height="11" width="2" x="3" y="6"/><rect height="11" width="2" x="19" y="6"/>
                </>}
                link="/eim/workflow"
            />

            <DNAppNavbarLink
                name="Фильтровать"
                iconRight={<>
                    <svg className="DNAppNavbarButtonInlineSVGRight" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                        <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                    </svg>
                </>}
                favicon={<>
                    <path fill="var(--dtcolor-header)" d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                </>}
                link="/eim/search"
            />

            <DNAppNavbarAction
                name="Создать"
                link="/eim/create"
            />
            
            <img className="DNAppNavbarElement DNAppNavbarAvatar DNAppNavbarElement-right" src={Avatar}/>
            
            <DNAppNavbarDropdown
            name="Default Account"
            className="DNAppNavbarElement-right"
            listClassName="DNAppNavbarDropdownOptionsListRight">
                <div className="DNAppNavbarDropdownSection">Мой аккаунт</div>
                <DNAppNavbarDropdownElement
                    name="Мой пользователь"
                    link="/admin/users/7PFFL2_UCvdMO8Av"
                />
                <DNAppNavbarDropdownElement
                    name="Мои обсуждения"
                    link="/eim/general/discuss"
                />
                
                <div className="DNAppNavbarDropdownSection">Дополнительно</div>
                
                <DNAppNavbarDropdownElement
                    name="Настройки"
                    link="/settings"
                />
                
                <DNAppNavbarDropdownElement
                    name="Выйти из системы"
                    link="/auth"
                    clickAction={<></>}
                />
            </DNAppNavbarDropdown>

            <NavLink to="/eim/help" className = "DNAppNavbarElement DNAppNavbarElement-right" activeClassName="DNAppNavbarElement DNAppNavbarElement-active">
                <svg className="DNAppNavbarButtonInline DNAppNavbarButtonInlineAll DNAppNavbarButtonInlineSVG" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"/></svg>
            </NavLink>

            <NavLink to="/eim/alerts" className = "DNAppNavbarElement DNAppNavbarElement-right" activeClassName="DNAppNavbarElement DNAppNavbarElement-active">
                <svg className="DNAppNavbarButtonInline DNAppNavbarButtonInlineAll DNAppNavbarButtonInlineSVG" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/>
                <path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-6v-5c0-3.07-1.63-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.64 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2zm-2 1H8v-6c0-2.48 1.51-4.5 4-4.5s4 2.02 4 4.5v6z"/></svg>
            </NavLink>

            
            <button onClick={()=>{
                const htmlTag = document.getElementsByTagName("html")[0]
                if (htmlTag.hasAttribute("data-theme")) {
                    htmlTag.removeAttribute("data-theme")
                    return
                }
                htmlTag.setAttribute("data-theme", "dark")
            }} className = "dt_button DNAppNavbarElement DNAppNavbarElement-right">
                <svg className="DNAppNavbarButtonInlineSVG" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="20" viewBox="0 0 24 24" width="20">
                    <path d="M19.78,17.51c-2.47,0-6.57-1.33-8.68-5.43C8.77,7.57,10.6,3.6,11.63,2.01C6.27,2.2,1.98,6.59,1.98,12 c0,0.14,0.02,0.28,0.02,0.42C2.61,12.16,3.28,12,3.98,12c0,0,0,0,0,0c0-3.09,1.73-5.77,4.3-7.1C7.78,7.09,7.74,9.94,9.32,13 c1.57,3.04,4.18,4.95,6.8,5.86c-1.23,0.74-2.65,1.15-4.13,1.15c-0.5,0-1-0.05-1.48-0.14c-0.37,0.7-0.94,1.27-1.64,1.64 c0.98,0.32,2.03,0.5,3.11,0.5c3.5,0,6.58-1.8,8.37-4.52C20.18,17.5,19.98,17.51,19.78,17.51z"/><path d="M7,16l-0.18,0C6.4,14.84,5.3,14,4,14c-1.66,0-3,1.34-3,3s1.34,3,3,3c0.62,0,2.49,0,3,0c1.1,0,2-0.9,2-2 C9,16.9,8.1,16,7,16z"/>
                </svg>
            </button>
        </DNAppNavbar>


        <Route exact path = "/eim" render={() => <Redirect to="/eim/documents"/>}/>

        <Route path = "/eim/documents/" component={EIMDocumentsView}/>
        <Route path = "/eim/documents/types/:id" component={EIMTemplateView}/>

        <Route exact path = "/eim/workflow/" component={EIMWorkflowSpaceView}/>
        <Route exact path = "/eim/workflow/:id" component={EIMWorkflowView}/>

        <Route path = "/eim/create" component={EIMCreateView}/>
        <Route path = "/eim/events">
            <DNAppContent hasMenu={false}
                backButtonLink="/eim/workflow/"
                backButtonName="Вернуться в процессы"
                header="События"
            >
                Событий нет
            </DNAppContent>
        </Route>

        <Route path = "/eim/help">
            <DNAppContent hasMenu={false}>
                <iframe width="670" height="400" src="https://www.youtube-nocookie.com/embed/r8RxkpUvxK0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                <DTAppContentHeaderLarge>  
                    Как создать документ
                </DTAppContentHeaderLarge>
                <br/>
                <iframe width="670" height="400" src="https://www.youtube-nocookie.com/embed/r8RxkpUvxK0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                <DTAppContentHeaderLarge>  
                    Как удалить документ
                </DTAppContentHeaderLarge>
            </DNAppContent>
        </Route>
    </div>
}

export default EIMMainView