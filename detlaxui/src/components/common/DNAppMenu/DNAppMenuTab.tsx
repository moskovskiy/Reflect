/* ---------------------------

    App Menu Navigation link
    @description: Provides NavLink with sent components

    Part of the Reflect UI system

---------------------------- */

// React imports
import React, {useState} from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

interface MenuTabProps {
    name: string;
    link: string;
    icon: any;
}

export default class DNAppMenuTab extends React.Component<MenuTabProps, {}> {
    constructor(props: MenuTabProps) {
        super(props);
    }

    render() {
        return (
            <>
            <NavLink to={this.props.link} className="DTAppMenuNavLink" activeClassName="DTAppMenuNavLinkActive">
                <button className="DTAppMenuNavigate">
                    <svg className="DTAppMenuNavigateIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                        {this.props.icon}
                    </svg>
                    <span className="DTAppMenuNavigateText">{this.props.name}</span>
                </button>
            </NavLink>
            </>
        );
    }
}