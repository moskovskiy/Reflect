import React from 'react';
import './DNAppNavbar.css';

import hubGlyph from '../../../assets/hub.png'
import eimGlyph from '../../../assets/eim.png'
import statsGlyph from '../../../assets/stats.png'
import settingsGlyph from '../../../assets/settings.png'
import externalGlyph from '../../../assets/external.png'

import DNAppNavbarDropdown from './DNAppNavbarDropdown'
import DNAppNavbarDropdownElement from './DNAppNavbarDropdownElement'

const DNAppNavbarMenu: React.FC = () => {
    return (
        
        <DNAppNavbarDropdown
        className="DNAppNavbarDropdownMenu"
        listClassName="DNAppNavbarDropdownMenuList"
        hasIcon={true}
        icon={
            <svg className="DNAppNavbarDropdownMenuIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M4 5.01C4 4.451 4.443 4 5.01 4h1.98C7.549 4 8 4.443 8 5.01v1.98C8 7.549 7.557 8 6.99 8H5.01C4.451 8 4 7.557 4 6.99V5.01zm0 6c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98C8 13.549 7.557 14 6.99 14H5.01C4.451 14 4 13.557 4 12.99v-1.98zm6-6c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98C14 7.549 13.557 8 12.99 8h-1.98C10.451 8 10 7.557 10 6.99V5.01zm0 6c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98c0 .558-.443 1.01-1.01 1.01h-1.98c-.558 0-1.01-.443-1.01-1.01v-1.98zm6-6c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98C20 7.549 19.557 8 18.99 8h-1.98C16.451 8 16 7.557 16 6.99V5.01zm0 6c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98c0 .558-.443 1.01-1.01 1.01h-1.98c-.558 0-1.01-.443-1.01-1.01v-1.98zm-12 6c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98C8 19.549 7.557 20 6.99 20H5.01C4.451 20 4 19.557 4 18.99v-1.98zm6 0c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98c0 .558-.443 1.01-1.01 1.01h-1.98c-.558 0-1.01-.443-1.01-1.01v-1.98zm6 0c0-.558.443-1.01 1.01-1.01h1.98c.558 0 1.01.443 1.01 1.01v1.98c0 .558-.443 1.01-1.01 1.01h-1.98c-.558 0-1.01-.443-1.01-1.01v-1.98z"></path>
            </svg>
        }>
            <div className="DNAppNavbarDropdownSectionLarge">Меню</div>
            <div className="DNAppNavbarDropdownSection DNAppNavbarDropdownSectionCard">Приложения Detlax</div>
            <DNAppNavbarDropdownElement
                name="Умный хаб"
                link="/hub"
                icon={hubGlyph}
                cardStyle={true}
            />
            <DNAppNavbarDropdownElement
                name="Документооборот"
                link="/eim"
                icon={eimGlyph}
                cardStyle={true}
            />
            <DNAppNavbarDropdownElement
                name="Отчёты и история"
                link="/stats"
                icon={statsGlyph}
                cardStyle={true}
            />
            <DNAppNavbarDropdownElement
                name="Администрирование"
                link="/admin"
                icon={settingsGlyph}
                cardStyle={true}
            />
            <div className="DNAppNavbarDropdownSection DNAppNavbarDropdownSectionCard">Внешние ссылки</div>
            <DNAppNavbarDropdownElement
                name="Salesforce"
                externalLink="https://salesforce.com"
                icon={externalGlyph}
                cardStyle={true}
            />
        </DNAppNavbarDropdown>
    )
}

export default DNAppNavbarMenu