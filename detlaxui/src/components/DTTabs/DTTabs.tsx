import React from 'react';
import './DTTabs.css';


const DTTabs: React.FC = (props) => {
    return <div className ="DTTabs">
            {props.children}
    </div>
}

export default DTTabs