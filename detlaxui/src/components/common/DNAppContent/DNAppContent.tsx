import React from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"
import './DNAppContent.css'

interface DNAppContentProps {
    hasMenu?: boolean;
    backButtonName?: any;
    backButtonLink?: string;
    header?: string;
    children: any;
}

const DNAppContent: React.FC<DNAppContentProps> = (props: DNAppContentProps) => {
    let backLink = "/"
    if (props.backButtonLink) backLink = props.backButtonLink
    return <div className={(props.hasMenu) ? "DNAppContent" : "DNAppContentNoLeftMenu"}>
        {props.backButtonName && <NavLink to={backLink}>
            <button className="dt_button dt_button-inline dt_imgbutton-container">
                <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
                <span className="dt_imgbutton-text">{props.backButtonName}</span>
            </button>
        </NavLink>
        }

        {props.header && <div className="DNAppContentHeaderLarge">
            {props.header}
        </div>}

        {props.children}
    </div>
}

export {
    DNAppContent,
}
