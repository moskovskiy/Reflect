import React from 'react';
import './Applications.css'
import DTAppContentNoMenu from '../components/DTAppContent/DTAppContentNoMenu'
import DTAppContentHeader from '../components/DTAppContent/DTAppContentHeader'

import { DNAppContent } from '../components/common/DNAppContent/DNAppContent'

import logo from '../assets/logo.png'
import eimGlyph from '../assets/eim.png'
import statsGlyph from '../assets/stats.png'
import settingsGlyph from '../assets/settings.png'
interface ApplicationsViewLinkProps {
    link: string;
    name: string;
    glyph: any;
}

const ApplicationsViewLink: React.FC<ApplicationsViewLinkProps> = (props) => {
    return <a href={props.link}>
        <div className="ApplicationsCard">
            <img className="ApplicationsGlyph" src={props.glyph} alt="EIM"/>
            <div className="ApplicationsCardHeader">
                {props.name}
            </div>
        </div>
    </a>
}

const ApplicationsView: React.FC = () => {
    document.title = 'Приложения – Detlax';
    return <>
        <DNAppContent
        hasMenu={false}
        header="Приложения">
            <div className="ApplicationsList"> 
                <ApplicationsViewLink
                    link="/eim"
                    name="Документооборот"
                    glyph={eimGlyph}
                />

                <a href="/stats">
                    <div className="ApplicationsCard">
                        <img className="ApplicationsGlyph" src={statsGlyph} alt="EIM"/>
                        <div className="ApplicationsCardHeader">
                         История и отчёты
                        </div>
                    </div>
                </a>
                <a href="/admin">
                    <div className="ApplicationsCard">
                        <img className="ApplicationsGlyph" src={settingsGlyph} alt="settings"/>
                        <div className="ApplicationsCardHeader">
                            Администрирование
                        </div>
                    </div>
                </a>
            </div>
        </DNAppContent>
    </>
}

export default ApplicationsView