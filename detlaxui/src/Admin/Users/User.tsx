import React from 'react';
import DTAppContentHeaderLarge from '../../components/DTAppContent/DTAppContentHeaderLarge'
import DTAppContentNoMenu from '../../components/DTAppContent/DTAppContentNoMenu'
import {BrowserRouter as withRouter} from 'react-router-dom'
import DTAppContentColumn from '../../components/DTAppContent/DTAppContentColumn'
import DTAppContentView from '../../components/DTAppContent/DTAppContentView'
import DTAppContentHeaderSmall from '../../components/DTAppContent/DTAppContentHeaderSmall'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';

const GetUserGraphQLQuery = gql`
    query GetUser ($payload: JSON!) {
	    eimQuery(name: "eim.User.Query.GetById", payload: $payload)
    } 
`

interface AdminUserDataId {
    id: String
}

const DeleteUserGraphQLMutation = gql`
    mutation ($name: String!, $payload: JSON!) {
	  eimCommand(name: $name, payload: $payload)
    } 
`

const AdminUserDataView: React.FC<AdminUserDataId> = (props) => {
    let id = props.id

    const [deleteUser] = useMutation(DeleteUserGraphQLMutation)
    
    let { loading, error, data } = useQuery(GetUserGraphQLQuery, {
        variables: { payload: "{\"id\":\"" + id + "\"}"},
      });

    if (loading) {
        return (<div className="dt_loader"></div>)
    }

    if (error) {
        return (<div>Ошибка загрузки данных</div>)
    }

    if (data) {
        try {
            const processed = JSON.parse(data.eimQuery)
            if (!processed.agentCR) {
                return <div>
                   <DTAppContentHeaderLarge>
                       404. Данного пользователя не существует
                    </DTAppContentHeaderLarge>
                </div>
            }

            document.title = processed.name + " " + processed.surname + ' - Detlax';
            return (<div>
            <DTAppContentHeaderLarge>
                Пользователь "{processed.name + " " + processed.surname}"
            </DTAppContentHeaderLarge>
            <div className="DTAppContentSeparator"/> 
            <DTAppContentView> 
                <DTAppContentColumn>
                    <DTAppContentHeaderSmall>Логин</DTAppContentHeaderSmall>
                    {(processed.login != "admin") ? 
                        <input type="text" className="dt_input-wide" value={processed.login}/> 
                        :
                        <span className="">{processed.login}</span>
                    }

                    <DTAppContentHeaderSmall>Имя</DTAppContentHeaderSmall>
                    <input type="text" className="dt_input-wide" value={processed.name}/>

                    <DTAppContentHeaderSmall>Фамилия</DTAppContentHeaderSmall>
                    <input type="text" className="dt_input-wide" value={processed.surname}/>
                    
                    <div className="dt_bottombuttons">
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-large dt_button-active">Обновить запись</button>
                    </div>
                </DTAppContentColumn>
                <DTAppContentColumn>
                    <DTAppContentHeaderSmall>Уникальный идентификатор</DTAppContentHeaderSmall>
                    <div className="dt_text-mono">{id}</div>

                    <DTAppContentHeaderSmall>Последнее изменение</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentLM}>{"Пользователем " + processed.agentLMName}</a></p>
                    <br/>
                    <p>{processed.dateLM}</p>

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Документ создан</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentCR}>{"Пользователем " + processed.agentCRName}</a></p>
                    <br/>
                    <p>{processed.dateCR}</p>

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Действия</DTAppContentHeaderSmall>
                    <div className="dt_bottombuttonsmenu">
                        {
                          (processed.login != "admin") ?
                            <button onClick={ () => {
                                    if (window.confirm("Вы действительно хотите удалить " + processed.name + "? Это действие невозможно отменить.")){
                                        deleteUser({ variables: { name: "eim.User.Command.Delete", payload: "{\"__ABSTRACT_SERVICE_action\":\"eim.User.Command.Delete\", \"userID\":\"" + id + "\"}" } })
                                        window.location.href="/admin/users"
                                    }
                                }}
                                className="dt_button dt_button-large dt_button-danger"
                                >Удалить пользователя
                            </button>
                             : <div className="dt_button dt_button-large dt_button-danger">Невозможно удалить admin</div>
                        }
                    </div>
                </DTAppContentColumn>
            </DTAppContentView>
            </div>)
        } catch(e) {
            console.log(e)
            return (<div>Ошибка распаковки JSON</div>)
        }
    }
    return (<div>Ошибка получения данных</div>)
}

function AdminUserView ({ match } : {match: any}) {
    let id = match.params.id

    return <div className ="AdminUserView">
        <DTAppContentNoMenu>
            <NavLink to="/admin/users">
                <button className="dt_button dt_button-inline dt_imgbutton-container">
                    <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
                    <span className="dt_imgbutton-text">Назад к пользователям</span>
                </button>
            </NavLink>
            <AdminUserDataView id={id}/>
        </DTAppContentNoMenu>
    </div>
}

export default AdminUserView