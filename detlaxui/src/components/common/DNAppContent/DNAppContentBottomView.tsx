import React from 'react'
import './DNAppContent.css'

const DNAppContentBottomView: React.FC = (props) => {
    return <div className ="DNAppContentBottomView">
            {props.children}
    </div>
}

export default DNAppContentBottomView