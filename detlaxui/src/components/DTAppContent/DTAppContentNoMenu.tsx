import React from 'react';
import './DTAppContent.css';

const DTAppContentNoMenu: React.FC = (props) => {
    return <div className = "DTAppContentNoLeftMenu">
        {props.children}
    </div>
}

export default DTAppContentNoMenu