import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import EIMMainView from './EIM/MainView';
import AdminMainView from './Admin/MainView'
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom"
import ApplicationsView from './Applications/Applications'
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/client';
import DNLoginView from './components/auth/DNLoginView';
import HubView from './Hub/MainView'

const client = new ApolloClient({
  uri: 'http://app.moskovskiy.org:8080/api',
  cache: new InMemoryCache()
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <Router>
        <div className="dt_okwide">
          <Route exact path = "/" render={() => <Redirect to="/eim" />} />
          <Route exact path = "/auth" component={DNLoginView} />
          <Route exact path = "/menu" component={ApplicationsView} />
          <Route path = "/eim" component={EIMMainView} />
          <Route path = "/admin" component={AdminMainView} />
          <Route path = "/hub" component={HubView} />
        </div>
      </Router>
    </ApolloProvider>
    <div className="dt_notokwide">
      <div className="dt_notwide-emoji">🖥️</div>
      <br/>
      <div className="dt_notwide">
        Экран этого устройства слишком маленький для работы c Detlax.
        <br/>
        Попробуйте повернуть экран или зайти с настольного компьютера.
        <br/><br/>
        Минимальная ширина экрана -&gt; 1000px &lt;-
      </div>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
