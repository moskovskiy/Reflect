import React from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

interface DNAppNavbarDropdownElementProps {
    name: string;
    link?: string;
    clickAction?: any;
    cardStyle?: boolean;
    externalLink?: string;
    icon?: any;
}

export default class DNAppNavbarDropdownElement extends React.Component<DNAppNavbarDropdownElementProps, {}> {
    constructor(props: DNAppNavbarDropdownElementProps) {
        super(props);
    }

    render() {
        let link ="*"
        if (this.props.link) link = this.props.link
        
        if (this.props.cardStyle) return (
            <>
                {this.props.externalLink && 
                <a href={this.props.externalLink}className="DNAppNavbarDropdownElement DNAppNavbarDropdownCardElement">
                    <img className="DNAppNavbarDropdownCardElementIcon" src={this.props.icon}/>
                    <div className="DNAppNavbarDropdownCardElementText">{this.props.name}</div>
                </a>
                }
                {(!this.props.externalLink) && <NavLink to={link} className="DNAppNavbarDropdownElement DNAppNavbarDropdownCardElement">
                    <img className="DNAppNavbarDropdownCardElementIcon" src={this.props.icon}/>
                    <div className="DNAppNavbarDropdownCardElementText">{this.props.name}</div>
                </NavLink>}
            </>
        )
        
        return (
            (link != "*")? <NavLink to={link} className="DNAppNavbarDropdownElement">
                <div className="DNAppNavbarDropdownText">{this.props.name}</div>
            </NavLink>:
            <div className="DNAppNavbarDropdownElement">
                <button className="DNAppNavbarDropdownText" onClick={this.props.clickAction}>{this.props.name}</button>
            </div>
        )
    }
}