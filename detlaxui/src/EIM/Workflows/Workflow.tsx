import React from 'react';
import {BrowserRouter as withRouter} from 'react-router-dom';
import DTAppContentColumn from '../../components/DTAppContent/DTAppContentColumn'
import DTAppContentView from '../../components/DTAppContent/DTAppContentView'
import DTAppContentNoMenu from '../../components/DTAppContent/DTAppContentNoMenu'
import DTAppContentHeaderLarge from '../../components/DTAppContent/DTAppContentHeaderLarge'
import DTAppContentHeaderSmall from '../../components/DTAppContent/DTAppContentHeaderSmall'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"
import './WorkflowSteps.css'
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';

const GetWorkflowGraphQLQuery = gql`
    query GetWorkflow ($payload: JSON!) {
	    eimQuery(name: "eim.Workflow.Query.GetById", payload: $payload)
    } 
`

interface EIMWorkflowDataId {
    id: String
}

const DeleteWorkflowGraphQLMutation = gql`
    mutation ($name: String!, $payload: JSON!) {
	  eimCommand(name: $name, payload: $payload)
    } 
`

class EIMWorkflowStepsView extends React.Component {
    render() {
        return (
            <>
            <table className="EIMWorkflowStepsContainer">
                <tr><td className="EIMWorkflowStep"><svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>         
    
                    <input type="text" className="dt_input-wide" placeholder="Что нужно сделать?"/>
                    
                    <div className="EIMDocumentDataViewWorkflowStepLabel">Минимальная роль для выполнения</div>
                    <select className="dt_input EIMDocumentDataViewWorkflowInsert">
                        <option value="volvo">Пользователь</option>
                        <option value="saab">Менеджер</option>
                        <option value="mercedes">Директор</option>
                        <option value="audi">Администратор</option>
                    </select>

                    
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_right">Добавить шаг ниже</button>
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_button-danger dt_right">Удалить шаг</button>
                    
                </td></tr>
                <tr><td className="EIMWorkflowStep"><svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>         
    
                    <input type="text" className="dt_input-wide" placeholder="Что нужно сделать?"/>
                    
                    <div className="EIMDocumentDataViewWorkflowStepLabel">Минимальная роль для выполнения</div>
                    <select className="dt_input EIMDocumentDataViewWorkflowInsert">
                        <option value="volvo">Пользователь</option>
                        <option value="saab">Менеджер</option>
                        <option value="mercedes">Директор</option>
                        <option value="audi">Администратор</option>
                    </select>

                    
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_right">Добавить шаг ниже</button>
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_button-danger dt_right">Удалить шаг</button>
                    
                </td></tr>
                <tr><td className="EIMWorkflowStep"><svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>         
    
                    <input type="text" className="dt_input-wide" placeholder="Что нужно сделать?"/>
                    
                    <div className="EIMDocumentDataViewWorkflowStepLabel">Минимальная роль для выполнения</div>
                    <select className="dt_input EIMDocumentDataViewWorkflowInsert">
                        <option value="volvo">Пользователь</option>
                        <option value="saab">Менеджер</option>
                        <option value="mercedes">Директор</option>
                        <option value="audi">Администратор</option>
                    </select>

                    
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_right">Добавить шаг ниже</button>
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_button-danger dt_right">Удалить шаг</button>
                    
                </td></tr>
                <tr><td className="EIMWorkflowStep"><svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>         
    
                    <input type="text" className="dt_input-wide" placeholder="Что нужно сделать?"/>
                    
                    <div className="EIMDocumentDataViewWorkflowStepLabel">Минимальная роль для выполнения</div>
                    <select className="dt_input EIMDocumentDataViewWorkflowInsert">
                        <option value="volvo">Пользователь</option>
                        <option value="saab">Менеджер</option>
                        <option value="mercedes">Директор</option>
                        <option value="audi">Администратор</option>
                    </select>

                    
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_right">Добавить шаг ниже</button>
                    <button className="dt_button EIMDocumentDataViewWorkflowInsert dt_button-danger dt_right">Удалить шаг</button>
                    
                </td></tr>
            </table>
            <button onClick={ () => {
                    
            }} className="dt_button dt_button-inline dt_button-large dt_button-secondary">Добавить шаг</button>   
            </>
        );
    }
}

const EIMWorkflowDataView: React.FC<EIMWorkflowDataId> = (props) => {
    let id = props.id

    const [deleteWorkflow] = useMutation(DeleteWorkflowGraphQLMutation)
    
    let { loading, error, data } = useQuery(GetWorkflowGraphQLQuery, {
        variables: { payload: "{\"id\":\"" + id + "\"}"},
      });
    /*
    if (loading) {
        return (<div className="dt_loader"></div>)
    }

    if (error) {
        return (<div>Ошибка загрузки данных</div>)
    }

    if (data) {*/
       // try {
            {/*const processed = JSON.parse(data.eimQuery)
            if (!processed.agentCR) {
                return <div>
                   <DTAppContentHeaderLarge>
                       404. Данного процесса не существует
                    </DTAppContentHeaderLarge>
                </div>
            }*/}
           // const processed = JSON.parse(data.eimQuery)

            document.title = ""//processed.name + ' - Detlax';
            return (<div>
            <DTAppContentHeaderLarge>
                Процесс {/*processed.name*/}
            </DTAppContentHeaderLarge>
            <DTAppContentView> 

            <div className="DTAppContentSeparator"/> 
            <DTAppContentColumn>
                <DTAppContentHeaderSmall>Шаги процесса</DTAppContentHeaderSmall>
                <span className="dt_hint">Опишите последовательность действий, которую необходимо сделать с документом. Шаги сохраняются автоматически после редактирования.</span>
                <EIMWorkflowStepsView/>{/* id={"1"}/> */}
                <div className="DTAppContentSeparator"/> 
                <br/>
                <DTAppContentHeaderSmall>Название</DTAppContentHeaderSmall>
                <input type="text" className="dt_input-wide" /> {/*value={processed.name}/>*/}

                <DTAppContentHeaderSmall>Описание процесса</DTAppContentHeaderSmall>
                <textarea className="dt_input-wide dt_input-tall"/>

                <div className="dt_bottombuttons">
                    <button onClick={ () => {
                        window.location.reload()
                    }} className="dt_button dt_button-inline dt_button-large dt_button-active">Сохранить запись</button>
                </div>

                </DTAppContentColumn>
                <DTAppContentColumn>
                    <DTAppContentHeaderSmall>Уникальный идентификатор</DTAppContentHeaderSmall>
                    <div className="dt_text-mono">{id}</div>
                    {/*
                    <DTAppContentHeaderSmall>Последнее изменение</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentLM}>{"Пользователем " + processed.agentLMName}</a></p>
                    <br/>
                    <p>{processed.dateLM}</p>

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Процесс создан</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentCR}>{"Пользователем " + processed.agentCRName}</a></p>
                    <br/>
                    <p>{processed.dateCR}</p>
                    */}
                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Действия</DTAppContentHeaderSmall>
                    <div className="dt_bottombuttonsmenu">
                        {
                            <button onClick={ () => {
                                    if (window.confirm("Вы действительно хотите удалить процесс? Это действие может затронуть документы и его невозможно отменить.")){
                                        deleteWorkflow({ variables: { name: "eim.Workflow.Command.Delete", payload: "{\"__ABSTRACT_SERVICE_action\":\"eim.Workflow.Command.Delete\", \"userID\":\"" + id + "\"}" } })
                                        window.location.href="/eim/workflows"
                                    }
                                }}
                                className="dt_button dt_button-large dt_button-danger"
                                >Удалить процесс
                            </button>
                        }
                    </div>
                </DTAppContentColumn>
            </DTAppContentView>
            </div>)
        /*} catch(e) {
            console.log(e)
            return (<div>Ошибка распаковки JSON</div>)
        }*/
    //}
    //return (<div>Ошибка получения данных</div>)
}

function EIMWorkflowView ({ match } : {match: any}) {
    let id = match.params.id

    return <div className ="EIMWorkflowView">
        <DTAppContentNoMenu>
            <NavLink to="/eim/workflow">
                <button className="dt_button dt_button-inline dt_imgbutton-container">
                    <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
                    <span className="dt_imgbutton-text">Назад к процессам</span>
                </button>
            </NavLink>
            <EIMWorkflowDataView id={id}/>
        </DTAppContentNoMenu>
    </div>
}

export default EIMWorkflowView