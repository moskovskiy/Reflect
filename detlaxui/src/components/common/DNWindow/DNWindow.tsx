/* ---------------------------

    App Menu Navigation link
    @description: Provides NavLink with sent components

    Part of the Reflect UI system

---------------------------- */

// React imports
import React from 'react';
import './DNWindow.css'

interface DNWindowProps {
    children: any;
    header: string;
    exitWindowAction: any;
    bottomPane: any;
    show: boolean;
}

type DNWindowState = {
}

export default class DNWindow extends React.Component<DNWindowProps, DNWindowState> {
    constructor(props: DNWindowProps) {
        super(props);
    }

    componentDidMount() {
        this.setState({
        });
    }

    render() {
        return(
        <>
        {
            this.props.show && 
            <div className ="DNWindowBg">
                <button className="DNWindowBgClick" onClick={
                    (ev: React.MouseEvent<HTMLButtonElement, MouseEvent>,): void => {
                        ev.stopPropagation()
                        this.props.exitWindowAction()
                    }
                }></button>
                <div className="DNWindow">
                    <button className="DNWindowClose" onClick={
                        (ev: React.MouseEvent<HTMLButtonElement, MouseEvent>,): void => {
                            ev.stopPropagation()
                            this.props.exitWindowAction()
                        }
                    }>
                        <svg fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M18.3 5.71c-.39-.39-1.02-.39-1.41 0L12 10.59 7.11 5.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z"/></svg>
                    </button>

                    <div className="DNWindowHeader">
                        {this.props.header}
                    </div>

                    <div className={this.props.bottomPane? "DNWindowContent" : "DNWindowContentNoBottomPane"}>
                        {this.props.children}
                    </div>
                    <div className="DNWindowBottomPane">
                        {this.props.bottomPane}
                    </div>
                </div>
            </div>
        }
        </>
        )
    }
}