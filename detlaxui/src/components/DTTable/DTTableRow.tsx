import React from 'react';
import './DTTable.css';

const DTTableRow: React.FC = (props) => {
    return <div className ="DTTableRow">
            {props.children}
    </div>
}

export default DTTableRow