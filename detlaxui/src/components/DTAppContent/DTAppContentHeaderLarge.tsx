import React from 'react';
import './DTAppContent.css';

const DTAppContentHeaderLarge: React.FC = (props) => {
    return <div className ="DTAppContentHeaderLarge">
        {props.children}
    </div>
}

export default DTAppContentHeaderLarge