import React from 'react';
import DTAppContentNoMenu from '../../components/DTAppContent/DTAppContentNoMenu'
import DTAppContentHeaderSmall from '../../components/DTAppContent/DTAppContentHeaderSmall'
import DTAppContentHeaderLarge from '../../components/DTAppContent/DTAppContentHeaderLarge'
import {useMutation} from '@apollo/react-hooks'
import gql from 'graphql-tag'

const CreateDocTemplateGraphQLMutation = gql`
    mutation ($name: String!, $payload: JSON!) {
	  eimCommand(name: $name, payload: $payload)
    } 
`

const EIMCreateDocTemplateView: React.FC = () => {
    document.title = 'Создать шаблон документа - Detlax';
    const [addDocument, { data }] = useMutation(CreateDocTemplateGraphQLMutation);
    return <>
    <DTAppContentNoMenu>
        
        <button className="dt_button dt_button-inline dt_imgbutton-container" onClick={() => window.history.back()}>
            <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
            <span className="dt_imgbutton-text">Вернуться</span>
        </button>

        <DTAppContentHeaderLarge>
            Создать шаблон документа
        </DTAppContentHeaderLarge>

        <DTAppContentHeaderSmall>Название</DTAppContentHeaderSmall>
        <input id="EIMCreateDocumentViewName" type="text" className="dt_input-wide"/>  
        <br/>
        <a href="/eim/documents/templates">
            <button className="dt_button dt_button-active EIMCreateSubmit" onClick={
                () => {
                    let name =  (document.getElementById("EIMCreateDocumentViewName") as HTMLInputElement).value
                    let content = (document.getElementById("EIMCreateDocumentViewContent") as HTMLInputElement).value
                    addDocument({ variables: { name: "eim.Document.Command.Create", payload: "{\"__ABSTRACT_SERVICE_action\":\"eim.Document.Command.Create\", \"name\":\"" + name + "\", \"content\":\"" + content + "\"}" } })
                }}>
                Создать
            </button>
        </a>

        <div className="DTAppContentSeparator"/>

        <DTAppContentHeaderSmall>Чтобы настроить шаблон, отредактируйте его после создания</DTAppContentHeaderSmall>
    </DTAppContentNoMenu>
    </>
}

export default EIMCreateDocTemplateView