/* ---------------------------

    App Menu Navigation link
    @description: Provides NavLink with sent components

    Part of the Reflect UI system

---------------------------- */

// React imports
import React from 'react';
import './DNLoginView.css'
import { DNAppContent } from '../common/DNAppContent/DNAppContent';
import logo from '../../assets/logo.png'
import { AuthorizationLogin } from '../../constants/auth'

interface LoginState {
    loggedIn: boolean;
    login: string;
    password: string;
    loginError: string;
}

export default class DNLoginView extends React.Component<{}, LoginState> {

    componentWillMount() {
        let loginraw = localStorage.getItem(AuthorizationLogin)
        let login : string = ""
        if (loginraw != null) login = loginraw

        this.setState({
            login: login,
            loggedIn: false,
            loginError: ""
        });
    }

    updateLogin (newLogin: string) {
        this.setState({
            login: newLogin
        })
    }

    updatePassword (newPassword: string) {
        this.setState({
            password: newPassword
        })
    }

    submitForm () {
        this.setState({
            loginError: "Ошибка 401. Неверный логин"
        })
        localStorage.setItem(AuthorizationLogin, this.state.login)
        document.location.href="/"
    }

    render() {
        document.title = 'Авторизация – Detlax';
        return (
            <div className="LoginView">
                <div className="LoginViewBackground"></div>
                <img className="LoginViewLogo" src={logo} alt="Detlax"/>
                <div className="LoginViewCard">
                    <br/>
                    {this.state.loginError && <div className="LoginViewInputErr">{this.state.loginError}</div>}
                    {!this.state.loginError && <div className="LoginViewInputName">Войдите, чтобы продолжить</div>}

                    <form>
                        <input type="text" placeholder="Логин" value={this.state.login} className="dt_input LoginViewInputField" autoFocus={this.state.login === ""} 
                        onChange={
                            (ev: React.ChangeEvent<HTMLInputElement>,): void => this.updateLogin(ev.target.value)
                        }/>

                        <input type="password" placeholder="Пароль" value={this.state.password} className="dt_input LoginViewInputField" autoFocus={this.state.login !== ""}
                        onChange={
                            (ev: React.ChangeEvent<HTMLInputElement>,): void => this.updatePassword(ev.target.value)
                        }/>

                        <button type="submit" onClick={
                            (ev: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
                                ev.preventDefault()
                                this.submitForm()
                            }} className="dt_button dt_button-active LoginViewSubmit">
                            Войти
                        </button>
                    </form>

                    <button className="dt_button dt_button-secondary LoginViewSubmit">Не удаётся войти?</button>
                </div>
            </div>
        );
    }
}