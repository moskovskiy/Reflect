import React from 'react';
import './DTAppContent.css';

const DTAppContentHeaderSmall: React.FC = (props) => {
    return <div className ="DTAppContentHeaderSmall">
        {props.children}
    </div>
}

export default DTAppContentHeaderSmall