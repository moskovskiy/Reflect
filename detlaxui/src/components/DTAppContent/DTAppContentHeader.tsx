import React from 'react';
import './DTAppContent.css';

const DTAppContentHeader: React.FC = (props) => {
    return <div className ="DTAppContentHeader">
        {props.children}
    </div>
}

export default DTAppContentHeader