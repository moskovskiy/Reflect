import React from 'react';
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"
import Avatar from '../assets/avatar.png'
import AdminUsersView from './Users/Users'
import AdminUserView from './Users/User'
import CreateUserView from './Users/CreateUserView'
import { DNAppContent } from '../components/common/DNAppContent/DNAppContent'
import { SystemVersion } from '../constants/version'

import DNAppNavbar from '../components/common/DNAppNavbar/DNAppNavbar'
import DNAppNavbarAction from '../components/common/DNAppNavbar/DNAppNavbarAction'
import DNAppNavbarMenu from '../components/common/DNAppNavbar/DNAppNavbarMenu'
import DNAppNavbarLink from '../components/common/DNAppNavbar/DNAppNavbarLink'
import DNAppNavbarDropdown from '../components/common/DNAppNavbar/DNAppNavbarDropdown'
import DNAppNavbarDropdownElement from '../components/common/DNAppNavbar/DNAppNavbarDropdownElement'

const AdminMainView: React.FC = () => {
    return <div className ="EIMMainView">
       <DNAppNavbar>
            <DNAppNavbarMenu/>
            <DNAppNavbarLink
                name="О системе"
                iconRight={<></>}
                favicon={<>
                    <path d="M17 1H7c-1.1 0-2 .9-2 2v18c0 1.1.9 2 2 2h10c1.1 0 2-.9 2-2V3c0-1.1-.9-2-2-2zm0 18H7V5h10v14zM12 6.72c-1.96 0-3.5 1.52-3.5 3.47h1.75c0-.93.82-1.75 1.75-1.75s1.75.82 1.75 1.75c0 1.75-2.63 1.57-2.63 4.45h1.76c0-1.96 2.62-2.19 2.62-4.45 0-1.96-1.54-3.47-3.5-3.47zM11 16h2v2h-2v-2z"/>
                </>}
                link="/admin/about"
            />

            <DNAppNavbarLink
                name="Пользователи"
                iconRight={<></>}
                favicon={<>
                    <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM7.07 18.28c.43-.9 3.05-1.78 4.93-1.78s4.51.88 4.93 1.78C15.57 19.36 13.86 20 12 20s-3.57-.64-4.93-1.72zm11.29-1.45c-1.43-1.74-4.9-2.33-6.36-2.33s-4.93.59-6.36 2.33C4.62 15.49 4 13.82 4 12c0-4.41 3.59-8 8-8s8 3.59 8 8c0 1.82-.62 3.49-1.64 4.83zM12 6c-1.94 0-3.5 1.56-3.5 3.5S10.06 13 12 13s3.5-1.56 3.5-3.5S13.94 6 12 6zm0 5c-.83 0-1.5-.67-1.5-1.5S11.17 8 12 8s1.5.67 1.5 1.5S12.83 11 12 11z"/>
                </>}
                link="/admin/users"
            />

            <DNAppNavbarLink
                name="Разрешения"
                iconRight={<></>}
                favicon={<>
                    <path d="M1.41 1.69L0 3.1l1 .99V16c0 1.1.89 2 1.99 2H10v2H8v2h8v-2h-2v-2h.9l6 6 1.41-1.41-20.9-20.9zM2.99 16V6.09L12.9 16H2.99zM4.55 2l2 2H21v12h-2.45l2 2h.44c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2H4.55z"/>
                </>}
                link="/admin/permissions"
            />

            <DNAppNavbarAction
                name="Создать"
                link="/admin/create"
            />

            <img className="DNAppNavbarElement DNAppNavbarAvatar DNAppNavbarElement-right" src={Avatar}/>
            <DNAppNavbarDropdown
            name="Default Account"
            className="DNAppNavbarElement-right"
            listClassName="DNAppNavbarDropdownOptionsListRight">

                <div className="DNAppNavbarDropdownSection">Действия</div>
                <DNAppNavbarDropdownElement
                    name="Выйти из системы"
                    link="/auth"
                    clickAction={<></>}
                />
            </DNAppNavbarDropdown>
        </DNAppNavbar>

        <Route exact path = "/admin" render={() => <Redirect to="/admin/users"/>}/>
        <Route exact path = "/admin/about" render={() => {return (
            <DNAppContent hasMenu={false}>
                Платформа Detlax<br/>
                Версия системы: {SystemVersion}
            </DNAppContent>
        )}}/>
        <Route exact path = "/admin/users"     component={AdminUsersView}/>
        <Route exact path = "/admin/users/:id" component={AdminUserView}/>
        <Route exact path = "/admin/create" component={CreateUserView}/>
    </div>
}

export default AdminMainView