/* ---------------------------

    App Menu Navigation link
    @description: Provides NavLink with sent components

    Part of the Reflect UI system

---------------------------- */

// React imports
import React, {useState} from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

interface MenuBackProps {
    text: string;
    link: string;
}

export default class DNAppMenuBack extends React.Component<MenuBackProps, {}> {
    constructor(props: MenuBackProps) {
        super(props);
    }

    render() {
        return (
            <>
                <NavLink to={this.props.link}>
                        <button className="DTAppMenuNavigate dt_button dt_button-inline dt_imgbutton-container DTAppMenuBackButton">
                            <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
                            <span className="dt_imgbutton-text">{this.props.text}</span>
                        </button>
                </NavLink>
            </>
        );
    }
}