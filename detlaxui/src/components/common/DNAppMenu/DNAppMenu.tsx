import React from 'react';
import './DNAppMenu.css';

const DNAppMenu: React.FC = (props) => {
    return <div className="DNAppMenu">
        {props.children}
    </div>   
}

export default DNAppMenu