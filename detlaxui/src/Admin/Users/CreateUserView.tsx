import React from 'react';
import DTAppContentHeaderSmall from '../../components/DTAppContent/DTAppContentHeaderSmall'
import {useQuery, useMutation} from '@apollo/react-hooks';
import DTAppContentNoMenu  from '../../components/DTAppContent/DTAppContentNoMenu'
import DTAppContentHeaderLarge from '../../components/DTAppContent/DTAppContentHeaderLarge'
import gql from 'graphql-tag';

const CreateUserGraphQLMutation = gql`
    mutation ($name: String!, $payload: JSON!) {
	  Command(name: $name, payload: $payload)
    } 
`

const GetUserGraphQLQuery = gql`
    query GetUser ($payload: JSON!) {
	    Query(name: ".User.Query.GetById", payload: $payload)
    } 
`

const CreateUserCheckLoginExists: React.FC = () => {
    let exists = true
    let id = ""
    let element = document.getElementById("CreateUserViewLogin") as HTMLInputElement
    
    if (element) {
        let id = (document.getElementById("CreateUserViewLogin") as HTMLInputElement).value
    }
    
    let { loading, error, data } = useQuery(GetUserGraphQLQuery, {
        variables: { payload: "{\"login\":\"" + id + "\"}"},
    });

    
    if (data) {
        const processed = JSON.parse(data.Query)
        exists = processed.exists
    }

    return <>{exists ? 
        <span className="dt_notok">× Логин уже занят </span>:
        <span className="dt_ok">✓ Логин уникальный </span> 
    }</>
}

const CreateUserView: React.FC = () => {
    const [addUser] = useMutation(CreateUserGraphQLMutation);

    document.title = 'Создать пользователя - Detlax';
    return <>

    <DTAppContentNoMenu>
        
        <button className="dt_button dt_button-inline dt_imgbutton-container" onClick={() => window.history.back()}>
            <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
            <span className="dt_imgbutton-text">Вернуться</span>
        </button>

        <DTAppContentHeaderLarge>
            Создать пользователя
        </DTAppContentHeaderLarge>

        <DTAppContentHeaderSmall>Логин</DTAppContentHeaderSmall>
        <input id="CreateUserViewLogin" type="text" className="dt_input-wide"/>  
        <CreateUserCheckLoginExists/>
        
        <DTAppContentHeaderSmall>Имя</DTAppContentHeaderSmall>
        <input id="CreateUserViewName" type="text" className="dt_input-wide"/>  

        <DTAppContentHeaderSmall>Фамилия</DTAppContentHeaderSmall>
        <input id="CreateUserViewSurname" type="text" className="dt_input-wide"/>  

        <br/>
        <a href="//users/">
            <button className="dt_button dt_button-active CreateSubmit" onClick={
                () => {
                    let name =  (document.getElementById("CreateUserViewName") as HTMLInputElement).value
                    let surname = (document.getElementById("CreateUserViewSurname") as HTMLInputElement).value
                    let login = (document.getElementById("CreateUserViewLogin") as HTMLInputElement).value
                    addUser({ variables: { name: ".User.Command.Create", payload: "{\"__ABSTRACT_SERVICE_action\":\".User.Command.Create\", \"name\":\"" + name + "\", \"surname\":\"" + surname + "\",  \"login\":\"" + login + "\"}" } })
                }}>
                Создать
            </button>
        </a>

    </DTAppContentNoMenu>
    </>
}

export default CreateUserView
