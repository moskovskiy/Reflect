import React from 'react';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom"
import './Create.css'
import folderGlyph from '../../assets/eim.png'
import documentGlyph from '../../assets/document.png'
import userGlyph from '../../assets/user.png'
import workflowGlyph from '../../assets/workflow.png'
import templateGlyph from '../../assets/template.png'

import DNWindow from '../../components/common/DNWindow/DNWindow'

import EIMCreateDocumentView from './EIMCreateDocumentView'
import EIMCreateUserView from '../../Admin/Users/CreateUserView'
import EIMCreateDocTemplateView from './EIMCreateDocTemplateView'


interface EIMCreateViewLinkProps {
    link: string;
    name: string;
    glyph: any;
}

class EIMCreateViewLink extends React.Component<EIMCreateViewLinkProps, {}> {
    render() {
        return(
            <NavLink to={this.props.link}>
                    <div className="CreateCard">
                        <img className="CreateGlyph" src={this.props.glyph} alt={this.props.name}/>
                        <div className="CreateCardHeader">
                            {this.props.name}
                        </div>
                    </div>
            </NavLink>
        )
    }
    
    constructor(props: EIMCreateViewLinkProps) {
        super(props)
    }
}

const EIMCreateView: React.FC = () => {
    document.title = 'Создать... - Detlax';
    return (
        <>
            <Route exact path = "/eim/create/document" render = {() =>
                <div>
                    <EIMCreateDocumentView/>
                </div>
            }/>

            <Route exact path = "/eim/create/doctemplate" render = {() =>
                <div>
                    <EIMCreateDocTemplateView/>
                </div>
            }/>

            <Route exact path = "/eim/create/workflow" render = {() =>
                <div>
                    Создание процессов пока не поддерживается
                </div>
            }/>

            <Route exact path = "/eim/create" render={() => 
                <>
                <DNWindow
                show={true}
                header="Создать..."
                exitWindowAction={
                    () => window.history.back()
                }
                bottomPane={<></>}>

                    <div className="EIMCreateViewCardSection">Документы</div>
                
                    <EIMCreateViewLink
                        name="Документ"
                        glyph={documentGlyph}
                        link="/eim/create/document"
                    />

                    <EIMCreateViewLink
                        name="Тип документа"
                        glyph={templateGlyph}
                        link="/eim/create/doctemplate"
                    />

                    <EIMCreateViewLink
                        name="Папку"
                        glyph={folderGlyph}
                        link="/eim/create/folder"
                    />

                    <div className="EIMCreateViewCardSection">Процессы</div>

                    <EIMCreateViewLink
                        name="Процесс"
                        glyph={workflowGlyph}
                        link="/eim/create/workflow"
                    />
                    </DNWindow>
                </>
            }/>
        </>
    )
}

export default EIMCreateView