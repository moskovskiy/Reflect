/* ---------------------------

    Template module
    @description: Displays view for single component

    Part of the Reflect UI system

---------------------------- */


// React imports
import React, {useState} from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

// GraphQL import
import {useQuery, useMutation} from '@apollo/react-hooks'
import gql from 'graphql-tag'

// Component import
import { DNAppContent } from '../../components/common/DNAppContent/DNAppContent'

// Old components import
import DTAppContentHeaderSmall from '../../components/DTAppContent/DTAppContentHeaderSmall'

// New components import 
import DNAppContentColumn from '../../components/common/DNAppContent/DNAppContentColumn'
import DNAppContentBottomView from '../../components/common/DNAppContent/DNAppContentBottomView'
import DNAppMenu from '../../components/common/DNAppMenu/DNAppMenu'
import DNAppMenuHeader from '../../components/common/DNAppMenu/DNAppMenuHeader'
import DNAppMenuTab from '../../components/common/DNAppMenu/DNAppMenuTab'
import DNAppMenuBack from '../../components/common/DNAppMenu/DNAppMenuBack'
import DNExpandView from '../../components/common/DNExpandView/DNExpandView'
import DNWindow from '../../components/common/DNWindow/DNWindow'

// CSS import
import './Template.css'

// Image import
import schemas from '../../assets/schemas.png'

// Interface for passing data to template
interface EIMTemplateDataId {
    id: String
}

export default EIMTemplateView
const EIMTemplateDataView : React.FC<EIMTemplateDataId> = (props) => {
    let id = props.id
    
    return (<div>
            <DNAppMenu>
                <DNAppMenuBack
                    link={"/eim/documents/types"}
                    text="Вернуться в типы"
                />

                <DNAppMenuHeader
                    name="Пример типа документа"
                    hint="Тип документа"
                />

                <DNAppMenuTab
                    link={"/eim/documents/types/"+id+"/schema"}
                    name="Схема карточки"
                    icon={
                        <><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 15v2H5v-2h14m2-10H3v2h18V5zm0 4H3v2h18V9zm0 4H3v6h18v-6z"/></>
                    }
                />

                <DNAppMenuTab
                    link={"/eim/documents/types/"+id+"/history"}
                    name="История изменений"
                    icon={
                        <>
                            <path d="M0 0h24v24H0V0z" fill="none"/><path d="M13 3c-4.97 0-9 4.03-9 9H1l3.89 3.89.07.14L9 12H6c0-3.87 3.13-7 7-7s7 3.13 7 7-3.13 7-7 7c-1.93 0-3.68-.79-4.94-2.06l-1.42 1.42C8.27 19.99 10.51 21 13 21c4.97 0 9-4.03 9-9s-4.03-9-9-9zm-1 5v5l4.25 2.52.77-1.28-3.52-2.09V8z"/>
                        </>
                    }
                />

                <DNAppMenuTab
                    link={"/eim/documents/types/"+id+"/comments"}
                    name="Обсуждение"
                    icon={
                        <>
                            <path d="M0 0h24v24H0V0z" fill="none"/><path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 14H5.17l-.59.59-.58.58V4h16v12zM6 12h2v2H6zm0-3h2v2H6zm0-3h2v2H6zm4 6h5v2h-5zm0-3h8v2h-8zm0-3h8v2h-8z"/>
                        </>
                    }
                />

                <DNAppMenuTab
                    link={"/eim/documents/types/"+id+"/properties"}
                    name="Дополнительно"
                    icon={
                        <>
                            <path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
                        </>
                    }
                />
            </DNAppMenu>

            <DNAppContent hasMenu={true}>  
                <Route exact path="/eim/documents/types/:id/schema" render={()=>{return <EIMTemplateSchemaView id={id}/>}}/>
                <Route exact path="/eim/documents/types/:id/properties" component={EIMTemplatePropertiesView}/>
                <Route exact path="/eim/documents/types/:id/" render={() => {document.location.href = document.location.href+"/schema"; return(<></>)}}/>
            </DNAppContent>
        </div>)
}

// GraphQL queries and mutations
const GetTemplateGraphQLQuery = gql`
    query GetTemplate ($payload: JSON!) {
	    eimQuery(name: "eim.Document.Query.GetById", payload: $payload)
    } 
`
const DeleteTemplateGraphQLMutation = gql`
    mutation ($name: String!, $payload: JSON!) {
	  eimCommand(name: $name, payload: $payload)
    } 
`

// Main component - wrapper
function EIMTemplateView ({ match } : {match:any}) {
    let id = match.params.id

    return <div className ="EIMTemplateView">
            <EIMTemplateDataView id={id}/>
    </div>
}

const EIMTemplateSchemaView : React.FC<EIMTemplateDataId> = (props) => {
    const [saved, setSaved] = useState(false)
    const [showChangePopup, setChangePopup] = useState(false)
    return (
        <>
            <DNAppContentColumn>
                <div className="EIMTemplateCover">
                    <div className="EIMTemplateContainer">
                        <button className="dt_button dt_button-large EIMTemplateField EIMTemplateAddButton">
                            <svg xmlns="http://www.w3.org/2000/svg" className="EIMTemplateAddButtonIcon" viewBox="0 0 24 24">
                                <path d="M14 10H2v2h12v-2zm0-4H2v2h12V6zm4 8v-4h-2v4h-4v2h4v4h2v-4h4v-2h-4zM2 16h8v-2H2v2z"/>
                            </svg>
                            <span className="EIMTemplateAddButtonLabel"> Добавить поле </span>
                        </button>
                        
                        <div className="EIMTemplateField">
                            <DNExpandView
                                label=""
                                labelElement={<>
                                    <button className="dt_button EIMTemplateFieldButtonDelete">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="EIMTemplateFieldButtonDeleteIcon" height="20" viewBox="0 0 24 24" width="20">
                                            <path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/>
                                        </svg>
                                        <span className="EIMTemplateFieldButtonDeleteText">
                                            Удалить поле из карточки
                                        </span>
                                    </button>
                                </>}
                                expandOpenHint="Показать настройки поля"
                                expandCloseHint="Свернуть настройки поля"
                                opened={<>
                                    <div className="EIMTemplateFieldHeader">Название поля</div>
                                    <input type="text" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <div className="EIMTemplateFieldHeader">Тип поля</div>

                                    <select className="dt_input EIMTemplateFieldType">
                                        <option onClick={()=>{alert("1")}} value="string">Строка</option>
                                        <option onClick={()=>{alert("2")}} value="int">Число</option>
                                        <option value="date">Дата</option>
                                        <option value="date">Дата и время</option>
                                        <option value="checkbox">Переключатель</option>
                                        <option value="doclink">Ссылка на документ</option>
                                        <option value="doctypelink">Ссылка на тип документа</option>
                                        <option value="userlink">Ссылка на пользователя</option>
                                        <option value="external">Внешняя ссылка</option>
                                    </select>

                                    <div className="EIMTemplateFieldHeader">Значение по умолчанию</div>
                                    <input type="date" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <br/>
                                </>}
                                closed={<>
                                    <div className="EIMTemplateFieldUpDown">
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M5,9l1.41,1.41L11,5.83V22H13V5.83l4.59,4.59L19,9l-7-7L5,9z"/></svg>
                                        </button>
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M19,15l-1.41-1.41L13,18.17V2H11v16.17l-4.59-4.59L5,15l7,7L19,15z"/></svg>
                                        </button>
                                    </div>
                                    <div className="EIMTemplateFieldHeaderLarge">Имя</div>
                                    <div className="EIMTemplateFieldDescription">Необходимо по умолчанию</div>
                                </>}
                            />
                        </div>
                        <div className="EIMTemplateField">
                            <DNExpandView
                                label=""
                                labelElement={<>
                                    <button className="dt_button EIMTemplateFieldButtonDelete">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="EIMTemplateFieldButtonDeleteIcon" height="20" viewBox="0 0 24 24" width="20">
                                            <path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/>
                                        </svg>
                                        <span className="EIMTemplateFieldButtonDeleteText">
                                            Удалить поле из карточки
                                        </span>
                                    </button>
                                </>}
                                expandOpenHint="Показать настройки поля"
                                expandCloseHint="Свернуть настройки поля"
                                opened={<>
                                    <div className="EIMTemplateFieldHeader">Название поля</div>
                                    <input type="text" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <div className="EIMTemplateFieldHeader">Тип поля</div>

                                    <select className="dt_input EIMTemplateFieldType">
                                        <option onClick={()=>{alert("1")}} value="string">Строка</option>
                                        <option onClick={()=>{alert("2")}} value="int">Число</option>
                                        <option value="date">Дата</option>
                                        <option value="date">Дата и время</option>
                                        <option value="checkbox">Переключатель</option>
                                        <option value="doclink">Ссылка на документ</option>
                                        <option value="doctypelink">Ссылка на тип документа</option>
                                        <option value="userlink">Ссылка на пользователя</option>
                                        <option value="external">Внешняя ссылка</option>
                                    </select>

                                    <div className="EIMTemplateFieldHeader">Значение по умолчанию</div>
                                    <input type="date" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <br/>
                                </>}
                                closed={<>             
                                    <div className="EIMTemplateFieldUpDown">
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M5,9l1.41,1.41L11,5.83V22H13V5.83l4.59,4.59L19,9l-7-7L5,9z"/></svg>
                                        </button>
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M19,15l-1.41-1.41L13,18.17V2H11v16.17l-4.59-4.59L5,15l7,7L19,15z"/></svg>
                                        </button>
                                    </div>
                                    <div className="EIMTemplateFieldHeaderLarge">Необходимо указать название</div>
                                    <div className="EIMTemplateFieldDescription">Необходимо по умолчанию</div>
                                </>}
                            />
                        </div>
                        <div className="EIMTemplateField">
                            <DNExpandView
                                label=""
                                labelElement={<>
                                    <button className="dt_button EIMTemplateFieldButtonDelete">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="EIMTemplateFieldButtonDeleteIcon" height="20" viewBox="0 0 24 24" width="20">
                                            <path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/>
                                        </svg>
                                        <span className="EIMTemplateFieldButtonDeleteText">
                                            Удалить поле из карточки
                                        </span>
                                    </button>
                                </>}
                                expandOpenHint="Показать настройки поля"
                                expandCloseHint="Свернуть настройки поля"
                                opened={<>
                                    <div className="EIMTemplateFieldHeader">Название поля</div>
                                    <input type="text" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <div className="EIMTemplateFieldHeader">Тип поля</div>

                                    <select className="dt_input EIMTemplateFieldType">
                                        <option onClick={()=>{alert("1")}} value="string">Строка</option>
                                        <option onClick={()=>{alert("2")}} value="int">Число</option>
                                        <option value="date">Дата</option>
                                        <option value="date">Дата и время</option>
                                        <option value="checkbox">Переключатель</option>
                                        <option value="doclink">Ссылка на документ</option>
                                        <option value="doctypelink">Ссылка на тип документа</option>
                                        <option value="userlink">Ссылка на пользователя</option>
                                        <option value="external">Внешняя ссылка</option>
                                    </select>

                                    <div className="EIMTemplateFieldHeader">Значение по умолчанию</div>
                                    <input type="date" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <br/>
                                </>}
                                closed={<>
                                    <div className="EIMTemplateFieldUpDown">
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M5,9l1.41,1.41L11,5.83V22H13V5.83l4.59,4.59L19,9l-7-7L5,9z"/></svg>
                                        </button>
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M19,15l-1.41-1.41L13,18.17V2H11v16.17l-4.59-4.59L5,15l7,7L19,15z"/></svg>
                                        </button>
                                    </div>
                                    <div className="EIMTemplateFieldHeaderLarge">Имя</div>
                                    <div className="EIMTemplateFieldDescription">Необходимо по умолчанию</div>
                                </>}
                            />
                        </div>
                        <div className="EIMTemplateField">
                            <DNExpandView
                                label=""
                                labelElement={<>
                                    <button className="dt_button EIMTemplateFieldButtonDelete">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="EIMTemplateFieldButtonDeleteIcon" height="20" viewBox="0 0 24 24" width="20">
                                            <path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/>
                                        </svg>
                                        <span className="EIMTemplateFieldButtonDeleteText">
                                            Удалить поле из карточки
                                        </span>
                                    </button>
                                </>}
                                expandOpenHint="Показать настройки поля"
                                expandCloseHint="Свернуть настройки поля"
                                opened={<>
                                    <div className="EIMTemplateFieldHeader">Название поля</div>
                                    <input type="text" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <div className="EIMTemplateFieldHeader">Тип поля</div>

                                    <select className="dt_input EIMTemplateFieldType">
                                        <option onClick={()=>{alert("1")}} value="string">Строка</option>
                                        <option onClick={()=>{alert("2")}} value="int">Число</option>
                                        <option value="date">Дата</option>
                                        <option value="date">Дата и время</option>
                                        <option value="checkbox">Переключатель</option>
                                        <option value="doclink">Ссылка на документ</option>
                                        <option value="doctypelink">Ссылка на тип документа</option>
                                        <option value="userlink">Ссылка на пользователя</option>
                                        <option value="external">Внешняя ссылка</option>
                                    </select>

                                    <div className="EIMTemplateFieldHeader">Значение по умолчанию</div>
                                    <input type="text" className="dt_input EIMTemplateFieldDefInput"></input>
                                    <input type="number" className="dt_input EIMTemplateFieldDefInput"></input>
                                    <input type="date" className="dt_input EIMTemplateFieldDefInput"></input>
                                    <input type="datetime-local" className="dt_input EIMTemplateFieldDefInput"></input>

                                    <br/>
                                </>}
                                closed={<>             
                                    <div className="EIMTemplateFieldUpDown">
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M5,9l1.41,1.41L11,5.83V22H13V5.83l4.59,4.59L19,9l-7-7L5,9z"/></svg>
                                        </button>
                                        <button className="dt_button">
                                            <svg className="EIMTemplateFieldUpDownIcon" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M19,15l-1.41-1.41L13,18.17V2H11v16.17l-4.59-4.59L5,15l7,7L19,15z"/></svg>
                                        </button>
                                    </div>
                                    <div className="EIMTemplateFieldHeaderLarge">Необходимо указать название</div>
                                    <div className="EIMTemplateFieldDescription">Необходимо по умолчанию</div>
                                </>}
                            />
                        </div>
                    
                    </div>
                </div>
                </DNAppContentColumn>
                <DNAppContentColumn>
                    <div className="DTAppContentColumnDivider">
                        <DTAppContentHeaderSmall>Настройте отображение карточки <br/> и доступ к редактированию для разных ролей</DTAppContentHeaderSmall>
                        <div className="dt_hint">Выбирайте, какие поля удобны для быстрого просмотра <br/> для всех, а какие нужны для <br/> редактирования только ответственным лицам</div>
                        <button className="dt_button dt_button-secondary dt_button-large">Настроить</button>
                        <br/>
                        <img className="DTAppContentImage" src={schemas}/>
                    </div>
                </DNAppContentColumn>

                <DNWindow
                show={showChangePopup}
                header="Подтвердите изменение документов"
                exitWindowAction={
                    () => setChangePopup(false)
                }
                bottomPane={<>
                    <button className="dt_button dt_button-active dt_button-large"
                    onClick={
                        () => {
                            setChangePopup(false)
                            setSaved(true)
                        }
                    }>
                    Сохранить</button>
                    
                    <button className="dt_button dt_button-inline dt_button-large"
                    onClick={
                        () => {
                            setChangePopup(false)
                        }
                    }>
                    Отмена</button>
                </>}>
                        Изменений нет
                </DNWindow>

                <DNAppContentBottomView>
                    <button onClick={() => {
                        setChangePopup(true)
                    }} className="dt_button dt_button-active dt_button-large EIMTemplateSave">Сохранить изменения</button>
                    <div className="EIMTemplateSaveHint">{
                       saved? "Изменения сохранены" : "Не забудьте сохранить изменения"
                    }</div>
                </DNAppContentBottomView>
        </>
    )
}

const EIMTemplatePropertiesView: React.FC<EIMTemplateDataId> = (props) => {
    let id = props.id
    return (<>

    <DTAppContentHeaderSmall>Название</DTAppContentHeaderSmall>
                    <input type="text" className="dt_input-wide"/>
                    <br/>
                    <button onClick={ () => {
                            window.location.reload()
                    }} className="dt_button dt_button-inline dt_button-large dt_button-active">Сохранить название</button>
                    
                    <br/>

                    <DTAppContentHeaderSmall>Уникальный идентификатор шаблона</DTAppContentHeaderSmall>
                    <div className="dt_text-mono">{id}</div>

                    {/*
                    
                    <DTAppContentColumn>

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Исходный документ создан</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentCR}>{"Пользователем " + processed.agentCRName}</a></p>
                    <br/>
                    <p>{processed.dateCR}</p>*/}

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Действия</DTAppContentHeaderSmall>
                    <div className="dt_bottombuttonsmenu">
                            <button onClick={ () => {
                                if (window.confirm("Вы действительно хотите удалить шаблон? Это действие невозможно отменить.")){
                                    window.location.href="/eim/documents"
                                }
                            }}
                            className="dt_button dt_button-large dt_button-danger">Удалить шаблон</button>
                    </div>
                    <br/>
    </>)
}

const EIMTemplateDataViewVersions : React.FC<EIMTemplateDataId> = (props) => {

    const [timeMark, switchMark] = useState(0);

    let switchTo = (tab: number) => {
        switchMark(tab)
    }
    {/*
            <DTAppContentHeaderSmall>Эта версия документа создана</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentLM}>{"Пользователем " + processed.agentLMName}</a></p>
                    <br/>
            <p>{processed.dateLM}</p>*/}  

    return (
        <div>
            <div className="EIMTemplateDataViewHistoryCard">
                <div className="dt_header EIMTemplateDataViewHistoryCardHeader">Tue, 20 May 2020 12:20 пользователем <a href={"/eim/users/"+"7PFFL2_UCvdMO8Av"}>{"admin"}</a></div>
                <div className="EIMTemplateDataViewHistoryCardRow">✏️️ Изменено поле: <span className="dt_text-mono">Название документа</span>, новое значение: <span className="dt_text-mono">5</span></div>
                <div className="EIMTemplateDataViewHistoryCardRow">✏️️ Изменено поле: <span className="dt_text-mono">Название документа</span>, новое значение: <span className="dt_text-mono">5</span></div>
                </div>
            <div className="EIMTemplateDataViewHistoryCard">
                <div className="dt_header EIMTemplateDataViewHistoryCardHeader">Tue, 20 May 2020 12:20 пользователем <a href={"/eim/users/"+"7PFFL2_UCvdMO8Av"}>{"admin"}</a></div>
                <div className="EIMTemplateDataViewHistoryCardRow">➕ Создан документ: <span className="dt_text-mono">Название документа</span></div>
            </div>
        </div>
    )
}
