import React from 'react';
import './DTAppContent.css';

const DTAppContentView: React.FC = (props) => {
    return <div className ="DTAppContentView">
        {props.children}
    </div>
}

export default DTAppContentView