import * as React from 'react';
import DTAppContentHeader from '../../components/DTAppContent/DTAppContentHeader'
import DTAppContentNoMenu from '../../components/DTAppContent/DTAppContentNoMenu';
import DTTable from '../../components/DTTable/DTTable'
import { BrowserRouter as Router, Route, NavLink, Redirect, useHistory } from "react-router-dom"

import Avatar from '../../assets/avatar.png'
import {useQuery} from '@apollo/react-hooks'
import gql from 'graphql-tag';

const GetUserGraphQLQuery = gql`
  query {
	  eimQuery(name: "eim.User.Query.GetAllNames", payload: "{}")
  } 
`

interface User {
    id: string
    name: string
    children: React.ReactNode
}

const AdminUsersLoader: React.FC = () => {
    let { loading, error, data } = useQuery(GetUserGraphQLQuery);

    if (loading) {
        return (<tr><td className="DTTableElement"><div className="dt_loader"></div></td></tr>)
    }
    if (error) {
        return (<tr><td className="DTTableElement">{error.message}</td></tr>)
    }
    var list: User[]
    try {
        list = JSON.parse(data.eimQuery).users
        
        return (<AdminUsersList users={list}/>)
    } catch(e) {
        console.log(e)
        return (<tr><td className="DTTableElement">
            Error parsing server response.
        </td></tr>)
    }
}

const AdminUserCard: React.FC<{ card: User }> = ({ card }) => {
    let history = useHistory();

    return <>
            <tr className="DTTableRow dt_selectable" key={card.id} onClick={() => {window.open("/eim/Users/"+card.id)}}>
                <td className="DTTableElement"><img className="DTTableElementAvatar" src={Avatar}/></td>
                <td className="DTTableElement">{card.name}</td>
                <td className="DTTableElement DTTableElementMono dt_text-mono">{card.id}</td>
            </tr>
        </>
}

const AdminUsersList: React.FC<{ users: User[] }> = ({ users }) => {
    return ((users.length == 0) ? <><td className="DTTableElement">Пользователей нет</td></>: <>
            {users.map(user => {
                return (<AdminUserCard key={user.id} card={user} />)
            })}
    </>)
}

interface AppState {
    count: number;
}

interface AppProps {
    steps: number;
    setUserToSearchRef: Element;
}

export default class AdminUsersView extends React.Component<AppProps, AppState> {
    private UserToSearch: React.RefObject<HTMLInputElement>;

    constructor(props: AppProps) {
        super(props);
        this.state = {
            count: 0
        };
        this.UserToSearch = React.createRef();
    }

    incrementCounter() {
        this.setState({count: this.state.count + 1});
    }

    render() {
    document.title = 'Пользователи - Detlax';
    return (
    <div className ="AdminUsersView">
        <DTAppContentNoMenu>
            <DTAppContentHeader>Пользователи</DTAppContentHeader>
            <div className="dt_br"/>

            <div className="DTAppContentPanel">
                <input type="text" className="dt_input" ref={this.UserToSearch} placeholder="Уникальный ID"></input>
                <NavLink to={() => {return "/eim/Users/" + this.UserToSearch.current}}>
                    <span className="dt_button dt_button-secondary DTAppContentPanelElementContainerMainLink">
                        <span className="DTAppContentPanelElement">Поиск</span>
                    </span>
                </NavLink>

                <button onClick={()=>{alert("Выбор сейчас недоступен")}} className="dt_button dt_button-inline DTAppContentPanelElementContainer">
                    <svg className="DTAppContentPanelElementSVG" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                        <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-8.29 13.29c-.39.39-1.02.39-1.41 0L5.71 12.7c-.39-.39-.39-1.02 0-1.41.39-.39 1.02-.39 1.41 0L10 14.17l6.88-6.88c.39-.39 1.02-.39 1.41 0 .39.39.39 1.02 0 1.41l-7.58 7.59z"/>
                    </svg>
                    <span className="DTAppContentPanelElement">Выбрать</span>
                </button>
                <button onClick={()=>{alert("Сортировка сейчас недоступна")}} className="dt_button dt_button-inline DTAppContentPanelElementContainer">
                    <svg className="DTAppContentPanelElementSVG" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                        <path d="M3 18h6v-2H3v2zM3 6v2h18V6H3zm0 7h12v-2H3v2z"/>
                    </svg>
                    <span className="DTAppContentPanelElement">Сортировать таблицу</span>
                </button>
                <button onClick={()=>{alert("Скачивание сейчас недоступно")}} className="dt_button dt_button-inline DTAppContentPanelElementContainer">
                    <svg className="DTAppContentPanelElementSVG" xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18">
                        <path d="M19 12v7H5v-7H3v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7h-2zm-6 .67l2.59-2.58L17 11.5l-5 5-5-5 1.41-1.41L11 12.67V3h2z"></path>
                    </svg>
                    <span className="DTAppContentPanelElement">Сохранить в CSV</span>
                </button>
            </div>
            <DTTable>
                <tr className="DTTableHeadRow">
                    <th className="DTTableElement DTTableHead">
                        Аватар
                    </th>
                    <th className="DTTableElement DTTableHead">
                        Имя
                    </th>
                    <th className="DTTableElement DTTableHead">
                        Уникальный идентификатор
                    </th>
                </tr>
                <AdminUsersLoader/>
            </DTTable>
        </DTAppContentNoMenu>
    </div>
    );
    }
}