import React from 'react';
import './DTAppContent.css';

const DTAppContent: React.FC = (props) => {
    return <div className = "DTAppContent">
        {props.children}
    </div>
}

export default DTAppContent