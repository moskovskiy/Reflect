import React from 'react';
import './DTAppMenu.css';

const DTAppMenu: React.FC = (props) => {
    return <div className="DTAppMenu">
        {props.children}
    </div>   
}

export default DTAppMenu