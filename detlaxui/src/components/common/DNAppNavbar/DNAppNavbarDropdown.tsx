import React from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"
import { open } from 'inspector';

interface DNAppNavbarDropdownState {
    opened: boolean;
}

interface DNAppNavbarDropdownProps {
    name?: string;
    opened?: boolean;
    children: any;
    className?: string;
    listClassName?: string;
    hasIcon?: boolean;
    icon?: any;
}

export default class DNAppNavbarDropdown extends React.Component<DNAppNavbarDropdownProps, DNAppNavbarDropdownState> {
    constructor(props: DNAppNavbarDropdownProps) {
        super(props);
        this.state = ({
            opened: false
        })
    }

    render() {
        const { opened } = this.state
        return (
            <>
                <div className={(this.props.hasIcon && "DNAppNavbarDropdown ") + "DNAppNavbarElement " + this.props.className}>
                    <button className={"dt_button DNAppNavbarButtonInline DNAppNavbarButtonInlinePE " + (opened && "DNAppNavbarButtonInlineHover")} onClick={
                        () => {
                            this.setState({
                                opened: !opened
                            })
                        }
                    }>
                        {this.props.hasIcon && this.props.icon}
                        {!this.props.hasIcon && this.props.name}
                    </button>
                </div>

                {opened && <div className="DNAppNavbarDropdownOptionsListWrapper" onClick={
                    () => {
                        this.setState({
                            opened: false
                        })
                    }
                }>
                    <div className={"DNAppNavbarDropdownOptionsList " + this.props.listClassName}>
                        {this.props.children}
                    </div>
                </div>}
            </>
        )
    }
}