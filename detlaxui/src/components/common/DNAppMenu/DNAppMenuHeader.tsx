/* ---------------------------

    App Menu Navigation link
    @description: Provides NavLink with sent components

    Part of the Reflect UI system

---------------------------- */

// React imports
import React, {useState} from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

interface MenuHeaderProps {
    name: string;
    hint: string;
}

export default class DNAppMenuHeader extends React.Component<MenuHeaderProps, {}> {
    constructor(props: MenuHeaderProps) {
        super(props);
    }

    render() {
        return (
            <>
                <div className="DTAppMenuHeader">
                    <div className="DTAppMenuHeaderUp">{this.props.hint}</div>
                    {this.props.name}
                </div>
            </>
        );
    }
}