import React from 'react';
import DTAppMenu from '../../components/DTAppMenu/DTAppMenu'
import DTAppContent from '../../components/DTAppContent/DTAppContent'
import DTAppContentHeader from '../../components/DTAppContent/DTAppContentHeader'
import { BrowserRouter as Router, Route, NavLink, Redirect, useHistory } from "react-router-dom"
import DTTable from '../../components/DTTable/DTTable';

import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';

const GetWorkflowGraphQLQuery = gql`
  query {
	  eimQuery(name: "eim.Workflow.Query.GetAllWorkflowsPreview", payload: "{}")
  } 
`

interface Workflow {
    id: string;
    name: string;
    contentpreview: string;
    dateLM: string;
    children: React.ReactNode
}

const EIMWorkflowsLoader: React.FC = () => {
    let { loading, error, data } = useQuery(GetWorkflowGraphQLQuery);

    if (loading) {
        return (<tr><td className="DTTableElement"><div className="dt_loader"></div></td></tr>)
    }
    if (error) {
        return (<tr><td className="DTTableElement">{error.message}</td></tr>)
    }
    var datamap: Workflow[]
    try {
        datamap = JSON.parse(data.eimQuery).documents
        return (<EIMWorkflowsList datamap={datamap}/>)
    } catch(e) {
        console.log(e)
        return (<tr><td className="DTTableElement">
            Error parsing server response.
        </td></tr>)
    }
}

const EIMWorkflowCard: React.FC<{ card: Workflow }> = ({ card }) => {
    let history = useHistory();
    return <>
            <tr className="DTTableRow dt_selectable" key={card.id} onClick={() => {history.push("/eim/documents/"+card.id)}}>
                {/*<td className="DTTableElement DTTableElementMono dt_text-mono">{card.id}</td>*/}
                <td className="DTTableElement">{card.name}</td>
                <td className="DTTableElement">{card.contentpreview}</td>
                <td className="DTTableElement">{"Документ существует"}</td>
                <td className="DTTableElement">{card.dateLM}</td>
            </tr>
        </>
}

const EIMWorkflowsList: React.FC<{ datamap: Workflow[] }> = ({ datamap }) => {
    return ((datamap.length == 0) ? <><td className="DTTableElement">Процессов нет</td></>: <>
        {datamap.map(document => {
            return (<EIMWorkflowCard key={document.id} card={document} />)
        })}
        </>
    )
}

interface AppState {
    count: number;
}

interface AppProps {
    steps: number;
    setdocumentToSearchRef: Element;
}

class EIMWorkflowsView extends React.Component<AppProps, AppState> {
    
    private documentToSearch: React.RefObject<HTMLInputElement>;

    constructor(props: AppProps) {
        super(props);
        this.state = {
            count: 0
        };
        this.documentToSearch = React.createRef();
    }

    incrementCounter() {
        this.setState({count: this.state.count + 1});
    }

    render() {
    return (
    <div className ="EIMWorkflowsView">
        <DTAppContent>
            <DTAppContentHeader>Процессы</DTAppContentHeader>
            <div className="dt_br"/>

            <DTTable>
                <tr className="DTTableHeadRow">
                    {/*<th className="DTTableElement DTTableHead">
                        Уникальный идентификатор
                    </th>*/}
                    <th className="DTTableElement DTTableHead">
                        Название
                    </th>
                    <th className="DTTableElement DTTableHead">
                        Шаги
                    </th>
                    <th className="DTTableElement DTTableHead">
                        Описание
                    </th>
                </tr>
                {/*<EIMWorkflowsLoader/>*/}
            </DTTable>
        </DTAppContent>
    </div>
    );
    }
}

const EIMWorkflowSpaceView: React.FC = () => {
    document.title = 'Процессы - Detlax';
    return <div className ="EIMWorkflowSpaceView">
            <DTAppMenu>
                <NavLink to="/eim/workflow/" className="DTAppMenuNavLink" activeClassName="DTAppMenuNavLinkActive">
                    <button className="DTAppMenuNavigate">Все процессы</button>
                </NavLink>

                <NavLink to="/eim/events" className="DTAppMenuNavLink" activeClassName="DTAppMenuNavLinkActive">
                    <button className="DTAppMenuNavigate">События</button>
                </NavLink>
            </DTAppMenu>
            <Route exact path = "/eim/workflow/" component={EIMWorkflowsView}/>
            <Route exact path = "/eim/events">
                <DTAppContent>
                    <DTAppContentHeader>
                        События
                    </DTAppContentHeader>
                </DTAppContent>
            </Route>
    </div>
}

export default EIMWorkflowSpaceView