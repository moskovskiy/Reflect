import React from 'react';
import './DTTable.css';


const DTTable: React.FC = (props) => {
    return <table className ="DTTable">
        <tbody>
            {props.children}
        </tbody>
    </table>
}

export default DTTable