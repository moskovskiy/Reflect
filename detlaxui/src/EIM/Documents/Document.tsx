import React, {useState} from 'react';
import DTAppContentHeaderLarge from '../../components/DTAppContent/DTAppContentHeaderLarge'
import DTAppContent from '../../components/DTAppContent/DTAppContent';
import {BrowserRouter as withRouter} from 'react-router-dom';
import DTAppContentColumn from '../../components/DTAppContent/DTAppContentColumn'
import DTAppContentView from '../../components/DTAppContent/DTAppContentView';
import DTAppContentHeaderSmall from '../../components/DTAppContent/DTAppContentHeaderSmall';
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"
import DTTabs from '../../components/DTTabs/DTTabs'

import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';

import './DocumentWorkflow.css'
import templateGlyph from '../../assets/template-inline.png'
import workflowGlyph from '../../assets/workflow-inline.png'

const GetDocumentGraphQLQuery = gql`
    query GetDocument ($payload: JSON!) {
	    eimQuery(name: "eim.Document.Query.GetById", payload: $payload)
    } 
`

interface EIMDocumentDataId {
    id: String
}

const DeleteDocumentGraphQLMutation = gql`
    mutation ($name: String!, $payload: JSON!) {
	  eimCommand(name: $name, payload: $payload)
    } 
`

const EIMDocumentDataViewVersions : React.FC<EIMDocumentDataId> = (props) => {

    const [timeMark, switchMark] = useState(0);

    let switchTo = (tab: number) => {
        switchMark(tab)
    }
    {/*
            <DTAppContentHeaderSmall>Эта версия документа создана</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentLM}>{"Пользователем " + processed.agentLMName}</a></p>
                    <br/>
            <p>{processed.dateLM}</p>*/}  

    return (
        <div>
            <div className="EIMDocumentDataViewHistoryCard">
                <div className="dt_header EIMDocumentDataViewHistoryCardHeader">Tue, 20 May 2020 12:20 пользователем <a href={"/eim/users/"+"7PFFL2_UCvdMO8Av"}>{"admin"}</a></div>
                <div className="EIMDocumentDataViewHistoryCardRow">✏️️ Изменено поле: <span className="dt_text-mono">Название документа</span>, новое значение: <span className="dt_text-mono">5</span></div>
                <div className="EIMDocumentDataViewHistoryCardRow">✏️️ Изменено поле: <span className="dt_text-mono">Название документа</span>, новое значение: <span className="dt_text-mono">5</span></div>
                </div>
            <div className="EIMDocumentDataViewHistoryCard">
                <div className="dt_header EIMDocumentDataViewHistoryCardHeader">Tue, 20 May 2020 12:20 пользователем <a href={"/eim/users/"+"7PFFL2_UCvdMO8Av"}>{"admin"}</a></div>
                <div className="EIMDocumentDataViewHistoryCardRow">➕ Создан документ: <span className="dt_text-mono">Название документа</span></div>
            </div>
        </div>
    )
}

const EIMDocumentDataView : React.FC<EIMDocumentDataId> = (props) => {
    let id = props.id
    const [deleteDocument] = useMutation(DeleteDocumentGraphQLMutation);

    const [currentTab, switchTab] = useState(0);

    let switchTo = (tab: number) => {
        switchTab(tab)
    }


    let { loading, error, data } = useQuery(GetDocumentGraphQLQuery, {
        variables: { payload: "{\"id\":\"" + id + "\"}"},
      });

    if (loading) {
        return (<div className="dt_loader"></div>)
    }

    if (error) {
        return (<div>Ошибка загрузки данных</div>)
    }

    if (data) {
        try {
            const processed = JSON.parse(data.eimQuery)
            if (!processed.agentCR) {
                return <div>
                    <DTAppContentHeaderLarge>
                        404. Данного документа не существует
                    </DTAppContentHeaderLarge>
                </div>
            }

            document.title = processed.name + ' - Detlax';
            return (<div>
            <DTAppContentHeaderLarge>
                {processed.name}
            </DTAppContentHeaderLarge>

            <DTTabs>
                <button className={(currentTab == 0)? "dt_button DTTabs-tab DTTabs-tab-active" : "dt_button DTTabs-tab"} onClick={()=>{switchTo(0)}}>Cведения</button>
                <button className={(currentTab == 3)? "dt_button DTTabs-tab DTTabs-tab-active" : "dt_button DTTabs-tab"} onClick={()=>{switchTo(3)}}>Поля документа</button>
                <button className={(currentTab == 2)? "dt_button DTTabs-tab DTTabs-tab-active" : "dt_button DTTabs-tab"} onClick={()=>{switchTo(2)}}>История изменений</button>
                <button className={(currentTab == 1)? "dt_button DTTabs-tab DTTabs-tab-active" : "dt_button DTTabs-tab"} onClick={()=>{switchTo(1)}}>Дополнительно</button>
            </DTTabs>
            <DTAppContentView>    
            {(currentTab == 0) && <>
                <div className="EIMDocumentDataViewWorkflowAlert">
                    <img src={workflowGlyph} className="EIMDocumentDataViewWorkflowAlert-icon"/>
                    <div className="EIMDocumentDataViewWorkflowAlert-name">Документ в процессе: Приказы внутренние</div>
                    <div className="EIMDocumentDataViewWorkflowAlert-status">Текущий статус: Ожидает подписания директора</div>

                    {/*<div className="dt_hint">У вашего аккаунта недостаточно прав для смены текущего статуса</div>
                    */}
                    <div className="dt_bottombuttons">
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-large dt_button-active">Подписать документ</button>
                        
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-large dt_button-secondary">Отклонить подпись</button>
                        
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-danger dt_button-large">Сменить процесс</button>
                    
                    </div>
                </div>
                <br/>
                <div className="EIMDocumentDataViewWorkflowAlert">
                    <img src={templateGlyph} className="EIMDocumentDataViewWorkflowAlert-icon"/>
                    <div className="EIMDocumentDataViewWorkflowAlert-name">Документ привязан к шаблону: Заявление сотрудника</div>
                    <div className="EIMDocumentDataViewWorkflowAlert-status">Отвяжите документ, чтобы его поля не изменялись при редактированити шаблона</div>

                    {/*<div className="dt_hint">У вашего аккаунта недостаточно прав для смены текущего статуса</div>
                    */}
                    <div className="dt_bottombuttons">
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-large dt_button-active">Редактировать шаблон</button>
                        
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-large dt_button-secondary">Сменить шаблон</button>
                        
                        <button onClick={ () => {
                            window.location.reload()
                        }} className="dt_button dt_button-inline dt_button-danger dt_button-large">Отвязать от шаблона</button>
                    
                    </div>
                </div>

                <br/>
                    {/*
                    <DTAppContentHeaderSmall>Процесс</DTAppContentHeaderSmall>
                    <div className="dt_hint">Нажмите этап на процессе, чтобы сменить статус</div>

                    <div className="dt_br"/>
                    <div className="EIMDocumentDataViewWorkflowContainer">
                        <div className="EIMDocumentDataViewWorkflowContainerElement EIMDocumentDataViewWorkflowContainerElement-active">
                            <svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>
                            <div className="EIMDocumentDataViewWorkflowStepText">Заявление подписано сотрудником</div>
                        </div>
                        <div className="EIMDocumentDataViewWorkflowContainerElement EIMDocumentDataViewWorkflowContainerElement-active">
                            <svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>
                            <div className="EIMDocumentDataViewWorkflowStepText">Сверка данных выполнена</div>
                        </div>
                        <div className="EIMDocumentDataViewWorkflowContainerElement EIMDocumentDataViewWorkflowContainerElement-active EIMDocumentDataViewWorkflowContainerElement-current">
                            <svg className="EIMDocumentDataViewWorkflowStepSVG-white" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" width="20"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2l-3.5-3.5c-.39-.39-1.01-.39-1.4 0-.39.39-.39 1.01 0 1.4l4.19 4.19c.39.39 1.02.39 1.41 0L20.3 7.7c.39-.39.39-1.01 0-1.4-.39-.39-1.01-.39-1.4 0L9 16.2z"/></svg>
                            <div className="EIMDocumentDataViewWorkflowStepText">Заявление подписано линейным менеджером</div>
                        </div>
                        <div className="EIMDocumentDataViewWorkflowContainerElement">
                            <div className="EIMDocumentDataViewWorkflowStepText">Приказ подписан у директора</div>
                        </div>
                        <div className="EIMDocumentDataViewWorkflowContainerElement">
                            <div className="EIMDocumentDataViewWorkflowStepText">Информация отправлена в РосРеестр</div>
                        </div>
                    </div>*/}
            </>
            }


            {(currentTab == 2) && <>
                <EIMDocumentDataViewVersions id ={id}/>
            </>
            }

            {(currentTab == 3) && <>

                <DTAppContentHeaderSmall>Поле</DTAppContentHeaderSmall>
                <input type="text" className="dt_input dt_input-wide"/>
                <br/>
                
                <DTAppContentHeaderSmall>Просмотр документа</DTAppContentHeaderSmall>
                <textarea className="dt_input-wide dt_input-tall" value={processed.content}/>
                <br/>

                <div className="dt_bottombuttons">
                    <button onClick={ () => {
                        window.location.reload()
                    }} className="dt_button dt_button-inline dt_button-large dt_button-active">Сохранить</button>
                </div>
            </>
            }


            {(currentTab == 1) && <>

                    <DTAppContentHeaderSmall>Название</DTAppContentHeaderSmall>
                    <input type="text" className="dt_input-wide" value={processed.name}/>
                    <br/>
                    <button onClick={ () => {
                            window.location.reload()
                    }} className="dt_button dt_button-inline dt_button-large dt_button-active">Сохранить название</button>
                    
                    <br/>

                    <DTAppContentHeaderSmall>Уникальный идентификатор документа</DTAppContentHeaderSmall>
                    <div className="dt_text-mono">{id}</div>

                    {/*
                    
                    <DTAppContentColumn>

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Исходный документ создан</DTAppContentHeaderSmall>
                    <p><a href={"/eim/users/"+processed.agentCR}>{"Пользователем " + processed.agentCRName}</a></p>
                    <br/>
                    <p>{processed.dateCR}</p>*/}

                    <div className="dt_br"/>
                    <DTAppContentHeaderSmall>Действия</DTAppContentHeaderSmall>
                    <div className="dt_bottombuttonsmenu">
                            <button onClick={ () => {
                                if (window.confirm("Вы действительно хотите удалить " + processed.name + "? Это действие невозможно отменить.")){
                                    deleteDocument({ variables: { name: "eim.Document.Command.Delete", payload: "{\"__ABSTRACT_SERVICE_action\":\"eim.Document.Command.Delete\", \"documentID\":\"" + id + "\"}" } })
                                    window.location.href="/eim/documents"
                                }
                            }}
                            className="dt_button dt_button-large dt_button-danger">Удалить документ</button>
                    </div>
                    <br/>
                    <div className="dt_hint">Документ будет удален из общего списка документов, однако все версии документа останутся доступны</div>
                    
                </>
            }
            </DTAppContentView>
            </div>)
        } catch(e) {
            console.log(e)
            return (<div>Ошибка распаковки JSON</div>)
        }
    }
    return (<div>Ошибка получения данных</div>)
}

function EIMDocumentView ({ match } : {match:any}) {
    let id = match.params.id

    return <div className ="EIMDocumentView">
        <DTAppContent>
            <NavLink to="/eim/documents">
                <button className="dt_button dt_button-inline dt_imgbutton-container">
                    <svg className="dt_imgbutton-svg" fill="var(--dtcolor-header)" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><rect fill="none" height="24" width="24"/><path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z"/></svg>
                    <span className="dt_imgbutton-text">Назад к документам</span>
                </button>
            </NavLink>
            <EIMDocumentDataView id={id}/>
        </DTAppContent>
    </div>
}

export default EIMDocumentView