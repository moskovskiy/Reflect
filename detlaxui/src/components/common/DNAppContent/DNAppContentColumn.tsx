import React from 'react'
import './DNAppContent.css'

const DNAppContentColumn: React.FC = (props) => {
    return <div className ="DNAppContentColumn">
        <div className="DNAppContentColumnView">
            {props.children}
        </div>
    </div>
}

export default DNAppContentColumn