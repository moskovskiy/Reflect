import React from 'react'
import { BrowserRouter as Router, Route, NavLink, Redirect } from "react-router-dom"

interface DNAppNavbarLinkProps {
    iconRight: any;
    favicon: any;
    name: string;
    link: string;
}

export default class DNAppNavbarLink extends React.Component<DNAppNavbarLinkProps, {}> {
    constructor(props: DNAppNavbarLinkProps) {
        super(props);
    }

    render() {
        return (
            <NavLink to={this.props.link} className = "DNAppNavbarElement" activeClassName="DNAppNavbarElement DNAppNavbarElement-active">
            <button className="dt_button DNAppNavbarButtonInline">
                {this.props.name}
                {this.props.iconRight}
            </button>
            <svg className="DNAppNavbarButtonInlineMini DNAppNavbarButtonInlineSVGIcon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                {this.props.favicon}
            </svg>
            </NavLink>
        )
    }
}